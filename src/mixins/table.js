const filterList = {
  data() {
    return {
      // 分页数据
      pagination: {
        page: 1,
        total: 4,
        pageSize: 3
      },
      // 表格列结构
      columns: [
        {
          key: 'date',
          name: '时间',
          minWidth: '80'
        },
        {
          key: 'name',
          name: '姓名',
          minWidth: '80'
        },
        {
          key: 'address',
          name: '地址',
          minWidth: '80'
        }
      ]
    };
  },

  methods: {
    pageChange() {
      return true;
    }
  }
};
export default filterList;
