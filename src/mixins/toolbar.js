const items = [
  {
    date: '2016-05-02',
    name: '王小虎',
    address: '上海市普陀区金沙江路 1518 弄'
  },
  {
    date: '2016-05-04',
    name: '王大虎',
    address: '上海市普陀区金沙江路 1517 弄'
  },
  {
    date: '2016-05-01',
    name: '王一虎',
    address: '上海市普陀区金沙江路 1519 弄'
  },
  {
    date: '2016-05-03',
    name: '王二虎',
    address: '上海市普陀区金沙江路 1516 弄'
  }
];

const field = {

  data() {
    return {
      // 搜索栏过滤条件
      filter: {
        input: '',
        radio: null
      },
      // 表格数据
      tableData: items
    };
  },

  methods: {
    reset() {
      this.tableData = items;
      this.filter = {
        input: '',
        radio: null
      };
    },
    search() {
      const list = this.tableData.filter(
        x => x.name.indexOf(this.filter.input) ^ -1
      );
      this.tableData = list;
    }
  }
};
export default field;
