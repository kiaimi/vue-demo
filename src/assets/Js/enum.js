class Enum {
  constructor(arr) {
    if (!(arr instanceof Array)) {
      // 初始化必须为数组
      throw new TypeError('Initialization must be a Array');
    }

    this.initData(arr);
  }

  // 初始值
  _arr = [];

  initData(arr) {
    this._arr = arr;
    this.initObj(arr);
  }

  initObj(arr) {
    let obj = {};
    arr.forEach(x => {
      let item = {
        getValue: () => x.value,
        getLabel: () => x.label
      };
      obj[x.key] = item;
    });
    // this._obj = obj;
    for (let i in obj) {
      this[i] = obj[i];
    }
    // this.init(obj);
  }

  // init(obj) {}

  /**
   * 通过中文文案获取其值
   * 可能少用
   * @param  {[type]} label [中文名]
   * @return {[type]}       [description]
   */
  getValueByLabel(label) {
    // 数组定义不正确 可能此语句报错
    const obj = this._arr.find(x => x.label === label);

    // 查无此键值对
    if (obj === undefined) {
      // 该数据似乎不存在
      // throw new Error('This data does not appear to exist');
      return obj;
    }

    return obj.value;
  }

  // /**
  //  * 通过英文文案获取其值
  //  * @param  {[type]} key [英文]
  //  * @return {[type]}       [description]
  //  */
  // getValueByKey(key) {
  //   // 数组定义不正确 可能此语句报错
  //   const index = this._arr.findIndex(x => x.key === key);

  //   // 查无此键值对
  //   if (index === -1) {
  //     // 该数据似乎不存在
  //     throw new Error('This data does not appear to exist');
  //   }

  //   return this._arr[index].value;
  // }

  /**
   * 反向通过值显示其中文文案
   * 页面展示会比较常用
   * @param  {[type]} value [值]
   * @return {[type]}       [description]
   */
  getLabelByValue(value) {
    // 数组定义不正确 可能此语句报错
    const obj = this._arr.find(x => x.value === value);

    // 查无此键值对
    if (obj === undefined) {
      // 该数据似乎不存在
      // throw new Error('This data does not appear to exist');
      return obj;
    }

    return obj.label;
  }

  /**
   * [键值对罗列，根据arr中的数据进行排序]
   * @param  {[type]} arr [key的一个数组集合]
   * @return {[type]}     [description]
   */
  getOptionArray(arr) {
    let result = [];

    // 默认值，根据初始的数组排序生成
    if (typeof arr === 'undefined') {
      result = this._arr.map(x => ({
        value: x.value,
        label: x.label
      }));
      return result;
    }

    if (arr instanceof Array) {
      arr.forEach(x => {
        if (this[x]) {
          result.push({
            value: this[x].getValue(),
            label: this[x].getLabel()
          });
        }
      });
      return result;
    }

    // 传入数据类型不正确
    throw new TypeError('Incorrect data type');
  }
}

const arr = [
  {
    value: 10,
    key: 'TOPAY',
    label: '待支付'
  },
  {
    value: 20,
    key: 'TODELIVER',
    label: '待发货'
  },
  {
    value: 30,
    key: 'INDELIVER',
    label: '配送中'
  },
  {
    value: 40,
    key: 'COMPLETED',
    label: '已完成'
  }
];
let menum = new Enum(arr);

menum.getLabelByValue(20);
menum.getOptionArray();
menum.getOptionArray(['COMPLETED', 'TOPAY', 'INDELIVER']);
menum.TOPAY.getValue();
menum.TOPAY.getLabel();
