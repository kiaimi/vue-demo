// let a = new Promise((resolve, reject) => {
//   resolve('success')
//   // reject('error');
// });
// a.then(res => {
//   console.log('success then', res);
//   throw new Error('api error');
// }).then(res2 => {
//   console.log('success2', res2)
// }).catch(err => {
//   console.error('error', err);
// });

// let b = new Promise((resolve, reject) => {
//   resolve('success')
//   // reject('error');
// });
// b.then(
//   res => {
//     console.log('success then', res);
//     return 'second'
//     // throw new Error('api error');
//   },
//   err => {
//     console.warn('error', err);
//   }
// ).then(res2 => {
//   console.log('success2', res2)
// }).catch(err => {
//   console.error('error', err);
// });
new Promise((resolve, reject) => {
  resolve('1');
});

class Promise {
  constructor(exec) {
    // 检验输入值
    if (typeof exec !== 'function') {
      throw new TypeError(`Promise resolve ${exec} is not a function`);
    }

    this.initValue();

    try {
      exec(this.resolve, this.reject);
    } catch (e) {
      this.reject(e);
    }
  }

  // 初始值
  state = Promise.PENDING; // 状态
  value = null; // 终值
  reason = null; // 拒绝原因
  onFulfilledCallbacks = []; // 成功回调缓存数组
  onRejectedCallbacks = []; // 失败回调缓存数组

  initValue() {
    this.state = Promise.PENDING;
    this.value = null;
    this.reason = null;
    this.onFulfilledCallbacks = [];
    this.onRejectedCallbacks = [];
  }

  // 处理成功操作后一系列操作(状态改变，成功回调执行)
  resolve = value => {
    if (this.state == Promise.PENDING) {
      this.state = Promise.FULFILLED;
      this.value = value;
      this.onFulfilledCallbacks.forEach(fn => fn(this.value));
    }
  };

  // 处理失败操作后一系列操作(状态改变，失败回调执行)
  reject = reason => {
    if (this.state == Promise.PENDING) {
      this.state = Promise.REJECTED;
      this.reason = reason;
      this.onRejectedCallbacks.forEach(fn => fn(this.reason));
    }
  };

  /**
   * 实现链式调用，且改变了后面的then的值，必须通过新的实例
   */
  then(onFulfilled, onRejected) {
    // 检验参数
    if (typeof onFulfilled !== 'function') {
      onFulfilled = value => value;
    }
    if (typeof onRejected !== 'function') {
      onRejected = reason => reason;
    }

    if (this.state === Promise.FULFILLED) {
      setTimeout(() => {
        try {
          const x = onFulfilled(this.value);
          this.resolve(x);
        } catch (e) {
          this.reject(e);
        }
      });
    }
    if (this.state === Promise.REJECTED) {
      setTimeout(() => {
        try {
          const x = onRejected(this.reason);
          this.resolve(x);
        } catch (e) {
          this.reject(e);
        }
      });
    }
    if (this.state === Promise.PENDING) {
      this.onFulfilledCallbacks.push(value => {
        setTimeout(() => {
          try {
            const x = onFulfilled(value);
            this.resolve(x);
          } catch (e) {
            this.reject(e);
          }
        });
      });
      this.onRejectedCallbacks.push(reason => {
        setTimeout(() => {
          try {
            const x = onRejected(reason);
            this.resolve(x);
          } catch (e) {
            this.reject(e);
          }
        });
      });
    }
  }
  all(promises) {
    const values = Array.apply(null, { length: promises.length }); // 用来保存所有成功的 value 的数组
    let resolveCount = 0;

    return new Promise((resolve, reject) => {
      promises.forEach((p, index) => {
        Promise.resolve(p).then(
          res => {
            resolveCount++;
            values[index] = res; // 避免不按顺序执行完成
            if (resolveCount === promises.length) {
              resolve(values);
            }
          },
          err => {
            reject(err);
          }
        );
      });
    });
  }
}

// 优化点： 魔法数字变成常量
Promise.PENDING = 'pending';
Promise.FULFILLED = 'fulfilled';
Promise.REJECTED = 'rejected';
