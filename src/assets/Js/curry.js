function add(a,b,c) {
  return a+b+c;
}


// /**
//  * 1.可以输入0~3个参数
//  */
// const curry = function (fn) {
//   let args = Array.prototype.slice.call(arguments, 1)
//   return function() {
//     let newArgs = args.concat(Array.prototype.slice.call(arguments))
//     return fn.apply(this, newArgs)
//   }
// }

// const addCurry = curry(add, 1, 2, 3);
// addCurry()


// /**
//  * 2.监听多层函数
//  * 然而没有尚未实现传参不按照顺序
//  */
// const curry = function (fn, args = []) {
//   let length = fn.length

//   return function() {
//     let _args = args.slice(0)
//     _args.push(...arguments)
//     if (_args.length < length) {
//       return curry.call(this, fn, _args)
//     } else {
//       return fn.apply(this, _args)
//     }
//   }
// }

// const addCurry = curry(add)
// addCurry(1,2,3)
// addCurry(1,2)(3)
// addCurry(1)(2)(3)
// addCurry(1)(2,3)
// addCurry(1,2,3,4)

/**
 * 3.收集空洞位置，填充上去
 * f(_)(1)
 * f(1,_)(2)
 * f(1,_,3)(2)
 * f(1)(_)(2)
 * f(_,_)(2)(3)(1)
 */
const _ = Symbol('empty') // {} // 一般不会传入参数使用Symbol 区别开options等于空对象尴尬
const checkLenth = (arr, length) => {
  const index = arr.findIndex(x => x === _)
  if (index === -1 && arr.length >= length) {
    return true
  } else if (index > length) {
    return true
  } else {
    return false
  }

}
const curry = function (fn, args = []) {
  let length = fn.length

  return function() {
    let _args = args.slice(0)
    let arr = Array.prototype.slice.call(arguments)

    /**
     * 这种写法会使 (1, _, 3) => (1,3)
     */
    // arr.forEach(item => {
    //   const index = _args.findIndex(x => x === _)
    //   if (index !== -1) {
    //     _args.splice(index, 1, item)
    //   } else {
    //     _args.push(item)
    //   }
    // })
    let temp = []
    arr.forEach(item => {
      const index = _args.findIndex(x => x === _)
      if (index !== -1) {
        _args.splice(index, 1, item)
      } else {
        temp.push(item)
      }
    })
    _args.push(...temp)
    if (!checkLenth(_args, length)) {
      return curry.call(this, fn, _args)
    } else {
      return fn.apply(this, _args)
    }
  }
}

const addCurry = curry(add)
addCurry(1,_,3)(2)
