// var num = 1;
// function a() {
//   var num = 100;
//   return function b() {
//     console.warn(this)
//     num ++;
//     return num;
//   }
// }

// var demo = a();
// demo()

let a = {
  name: 'loki',
  info: {
    school: 'abc',
    grade: 12
  }
};

let b = a;
let c = Object.assign({}, a);
let d = JSON.parse(JSON.stringify(a));

a.name = 'john';
console.log(b.name);
console.warn(c.name);
console.error(d.name);

a.info.school = 'bcd';
console.log(b.info.school);
console.warn(c.info.school);
console.error(d.info.school);

a.info.no = 12;
console.log(b.info.no);
console.warn(c.info.no);
console.error(d.info.no);

// BigInt 导文
// let a = BigInt(1)
// let b= '1n'

// a==b // false
// a==1 // true
// a===1 // false
// a.toString() // '1'

// Symbol 导文
// let num = 1;
// let a = Symbol(num);
// let b = Symbol(1);
// let c = Symbol(num);
// a === b; // false
// a === c; // false
// a.description === b.description; // true

let str = 'body';
let a = Symbol.for(str);
let b = Symbol.for('body');
let c = Symbol('body');

a === b; // ture
a === c; // false

Symbol.keyFor(a); // 'body'
Symbol.keyFor(c); // undefined
