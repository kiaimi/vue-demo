/**
 * 主初始函数
 * @param  {[type]} vm [description]
 * @return {[type]}    [description]
 */
function initStatue(vm) {
  vm._watchers = [];
  var opts = vm.$options;

  // 初始化props
  if (opts.props) {
    initProps(vm, opts.props);
  }
  // 初始化methods
  if (opts.methods) {
    initMethods(vm, opts.methods);
  }
  // 初始化data
  if (opts.data) {
    initData(vm);
  } else {
    // 如果没有定义data, 则创建一个空对象, 并设置为响应式
    // 预防使用this.$set() 没有根节点绑定
    observe((vm._data = {}), true /* 作为根数据 */);
  }
  // 初始化computed
  if (opts.computed) {
    initComputed(vm, opts.computed);
  }
  // 初始化watch
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch);
  }
}

/**
 * 初始化传入属性
 * @param  {[type]} vm           [description]
 * @param  {[type]} propsOptions [description]
 * @return {[type]}              [description]
 */
function initProps(vm, propsOptions) {
  var propsData = vm.$options.propsData || {};
  var loop = function(key) {
    // ...
    defineReative(props, key, value, cb);
    // 检验是否存在同名属性
    if (!(key in vm)) {
      proxy(vm, '_props', key);
    }
  };
  for (let key in propsOptions) loop(key);
}

/**
 * 初始化函数
 * 1.保证method方法必须为函数
 * 2.和上面定义的属性名不重复
 * @param  {[type]} vm      [description]
 * @param  {[type]} methods [description]
 * @return {[type]}         [description]
 */
function initMethods(vm, methods) {
  var props = vm.$options.props;
  for (let key in methods) {
    {
      // method必须为函数
      if (typeof methods[key] !== 'function') {
        throw new Error();
      }
      // 不能重复字段名
      if (props && hasOwn(props, key)) {
        throw new Error();
      }
      // 不能以_/$这些Vue的保留标志开头
      if (key in vm && isReserved(key)) {
        throw new Error();
      }
    }
    // 挂载到实例属性上，可以直接调用vm[method]使用
    vm[key] =
      typeof methods[key] !== 'function' ? noop : bind(methods[key], vm);
  }
}

/**
 * 初始化数据
 * 1.和上述数据不重复定义
 * @param  {[type]} vm [description]
 * @return {[type]}    [description]
 */
function initData(vm) {
  var data = vm.$options.data;
  // 根实例时, data是一个对象, 子组件的data是一个函数, 其中getData会调用函数返回data对象
  data = vm._data = typeof data = 'function' ? getData(data, vm) : data || {};
  var key = Object.keys(data);
  var props = vm.$options.props;
  var methods = vm.$options.methods;
  for (let ket in data) {
    {
      // 命名和方法重复
      if (methods && hasOwn(methods, key)) {
        throw new Error();
      }
      // 命名不能和props重复
      if (props && hasOwn(props, key)) {
        throw new Error();
      } else if (!isReserved(key)) {
        // 数据代理，用户可以直接通过vm实例返回data数据
        proxy(vm, '_data', key);
      }
    }
  }
  // 监听数据;
  observe(data, true);
}

function initComputed(vm, computed) {
  // ...
  for (let i in computed) {
    var userDef = computed[key];
    var getter = typeof userDef === 'function' ? userDef : userDef.get;
    // computed属性为对象时，要保证有getter方法
    if (getter == null) {
      throw new Error();
    }
    if (!isSSR) {
      // 创建computed watcher
      watcher[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      );
    }
    if (!(key in vm)) {
      defineComputed(vm, key, userDef);
    } else {
      // 不能和props, data命名冲突
      if (key in vm.$data) {
        throw new Error();
      } else if (vm.$options.props && key in vm.$options.props) {
        throw new Error();
      }
    }
  }
}

// 空函数
const noop = () => {};

// export function proxy(target: Object, sourceKey: string, key: string) {
//   sharedPropertyDefinition.get = function proxyGetter() {
//     return this[sourceKey][key];
//   };
//   sharedPropertyDefinition.set = function proxySetter(val) {
//     this[sourceKey][key] = val;
//   };
//   Object.defineProperty(target, key, sharedPropertyDefinition);
// }

// defineComputed
// isReserved
// getData
// hasOwn
// observe
