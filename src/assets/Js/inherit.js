function Person(name) {
  this.name = name || 'Loki';
}

Person.prototype.getName = function() {
  console.log(`this guy name is ${this.name}`);
};

/**
 * 1.原型链继承
 * 无法类似父类直接声明传参
 */
function Man() {
  this.age = 12;
}
Man.prototype = new Person();
Man.prototype.constructor = Man;

let man = new Man(99);
man.getName(); // 'this guy name is Loki'

man.constructor; // 不同

man.age; // 12 & 12

/**
 * 2.经典继承
 * 无法使用父类的方法
 */
function Woman(name) {
  Person.call(this, name);
}

let woman = new Woman('Lily');
woman.name; // Lily
woman.getName(); // TypeError

/**
 * 3.组合继承
 * 混合两者，解决两者的缺点
 */
function Zombie(name) {
  Person.call(this, name);
}
Zombie.prototype = new Person();

let zombie = new Zombie('Babo');
zombie.name; // Babo
zombie.getName(); // 'this guy name is Babo'

/**
 * 4.原型式继承
 * 模拟Es5 Object.create 的实现
 * 缺点 与原型链一样，所有子类的公共属性引用父类的
 */
function createObj(o) {
  function F() {}
  F.prototype = o;
  return new F();
}

var person = {
  name: 'kevin',
  friends: ['daisy', 'kelly']
};

var person1 = createObj(person);
person1.name;

// /**
//  * 5.寄生式继承
//  * 复制出父类原型副本
//  * 重新赋值constructor
//  */
function createObj(o) {
  // 第四点模拟实现
  var clone = Object.create(o);
  clone.sayName = function() {
    console.log('hi');
  };
  return clone;
}

/**
 * 6.寄生组合式继承
 */
function Parent() {
  this.name = 'loki';
}
function Child() {
  Parent.call(this);
  this.age = 18;
}
function object(o) {
  function F() {}
  F.prototype = o;
  return new F();
}

function prototype(child, parent) {
  var prototype = object(parent.prototype);
  prototype.constructor = child;
  child.prototype = prototype;
}

prototype(Child, Parent);

let child = new Child();
