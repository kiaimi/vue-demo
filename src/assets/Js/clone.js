// let str = 'abc';
// let str1 = new String(str);
// let str2 = String(str);
// str1;
// str2;
// console.log(str1.toString());
// console.log(str2.toString());
// console.log(typeof str1);
// console.log(typeof str2);
// console.log(Object.prototype.toString.call(str1));
// console.log(Object.prototype.toString.call(str2));

/**
 * 创建指定深度和广度的对象
 * https://segmentfault.com/a/1190000016672263
 * @param  {[type]} deep    [description]
 * @param  {[type]} breadth [description]
 * @return {[type]}         [description]
 */
function createData(deep, breadth) {
  var data = {};
  var temp = data;

  for (var i = 0; i < deep; i++) {
    temp = temp['data'] = {};
    for (var j = 0; j < breadth; j++) {
      temp[j] = j;
    }
  }

  return data;
}

/**
 * 暂不考虑基本类型转使用 new 换成引用类型，基本类型的原型链没有更改原数据的方法
 */

const cloneShallow = source => {
  /*
  原备选选择类型有
  1. typeof => 使用 new 方法新建的基本类型都是'object'
  2. instanceof
   */
  const type = Object.prototype.toString.call(source);
  switch (type) {
    case '[object Array]':
      return cloneShallowOther(source, 'array');
    case '[object Object]':
      return cloneShallowOther(source, 'object');
    default:
      return source;
  }
};

const cloneShallowOther = (source, type = 'object') => {
  let target = {};
  if (type === 'array') target = [];
  for (let key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      target[key] = source[key];
    }
  }
  return target;
};

const cloneDeep = source => {
  let target;
  const type = Object.prototype.toString.call(source);
  switch (type) {
    case '[object Array]':
      target = [];
      break;
    case '[object Object]':
      target = {};
      break;
    default:
      return source;
  }
  for (let key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      target[key] = cloneDeep(source[key]);
    }
  }
  return target;
};

/**
 * [原基础上加入hash表,保证引入类型的指向相同]
 * @param  {[type]}  source [description]
 * @param  {WeakMap} hash   [弱引用，垃圾回收机制执行]
 * @return {[type]}         [description]
 */
const cloneDeep2 = (source, hash = new WeakMap()) => {
  // 判断是否存在哈希表中,直接使用
  if (hash.has(source)) return hash.get(source);
  let target;
  const type = Object.prototype.toString.call(source);
  switch (type) {
    case '[object Array]':
      target = [];
      break;
    case '[object Object]':
      target = {};
      break;
    default:
      return source;
  }
  for (let key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      target[key] = cloneDeep2(source[key], hash);
    }
  }
  hash.set(source, target);
  return target;
};

/**
 * 判断是否引用类型 Array/
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
const isQuote = data => {
  const type = Object.prototype.toString.call(data);
  switch (type) {
    case '[object Array]':
      return true;
    case '[object Object]':
      return true;
    default:
      return false;
  }
};

/**
 * 初始化类型
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
const initTemp = data => {
  const type = Object.prototype.toString.call(data);
  switch (type) {
    case '[object Array]':
      return [];
    case '[object Object]':
      return {};
    default:
      throw new Error('类型不正确,此处应为引用类型Array/Object');
  }
};

/**
 * 使用遍历方法, 广度遍历
 * 无法实现 hash表 指针两个指向同一个对象
 * 已解决 指针同一个对象
 * @param  {[type]} source [description]
 * @return {[type]}        [description]
 */
const cloneDeep3 = source => {
  /* 初始化数据 */
  const hash = new WeakMap();
  if (!isQuote(source)) {
    return source;
  }
  let root = initTemp(source);
  const loopList = [
    {
      parent: root,
      key: undefined,
      data: source
    }
  ];

  while (loopList.length) {
    const node = loopList.pop();
    const parent = node.parent;
    const key = node.key;
    const data = node.data;

    let res = root;
    if (hash.has(data)) {
      res = parent[key] = hash.get(data);
    } else if (typeof key !== 'undefined') {
      // 非根节点
      res = parent[key] = initTemp(data);
    }

    for (let k in data) {
      if (isQuote(data[k])) {
        // 引用类型
        // 不考虑相同引用类型
        // loopList.push({
        //   parent: res,
        //   key: k,
        //   data: data[k]
        // });
        if (hash.has(data[k])) {
          res[k] = hash.get(data[k]);
        } else {
          loopList.push({
            parent: res,
            key: k,
            data: data[k]
          });
          hash.set(data[k], data[k]);
        }
      } else {
        // 普通类型
        res[k] = data[k];
      }
    }
  }
  return root;
};

// test Data

// test one 改变原对象，对应值是否更改
// let arr = [1, 2, { obj: '999', age: 12 }, [998]];
// let sArr = cloneShallow(arr);
// let dArr = cloneDeep(arr);

// let obj = {
//   name: 'loki',
//   age: 12,
//   info: {
//     school: 'abc',
//     grade: 1
//   }
// };
// let sObj = cloneShallow(obj);
// let dObj = cloneDeep(obj);

// arr[0] = 'john';
// arr[2].age = 60;

// console.log('sArr', sArr);
// console.warn('dArr', dArr);

// obj.name = 'jam';
// obj.info.grade = 6;
// console.log('sObj', sObj);
// console.warn('dObj', dObj);

// // test two 引用类型不再相等
let obj1 = {};
let obj = {};
let obj2 = {
  a: obj1,
  b: obj
};

let obj3 = cloneDeep(obj2);
obj2.a === obj2.b; // true
obj3.a === obj3.b; // false

let obj4 = cloneDeep2(obj2);
obj2.a === obj2.b; // true
obj4.a === obj4.b; // true

// // test three 基本类型Symbol
// let obj = {
//   sign: Symbol('857')
// };
// let obj1 = cloneDeep2(obj);
// obj1.sign === obj.sign;

// test four 检测循环
// let a = {};
// a.a = a;
// let b = cloneDeep2(a) // VM41:93 Uncaught RangeError: Maximum call stack size exceeded
// let b = cloneDeep3(a) // VM41:93 Uncaught RangeError: Maximum call stack size exceeded
/**
 *  此处没有使用检测死循环，导致自己调用自己超出堆栈，万一无限制则会内存溢出宕机
 */

// test five 是否溢出
// cloneDeep2(createData(10000)) // VM185:93 Uncaught RangeError: Maximum call stack size exceeded
// cloneDeep3(createData(10000)) // ok
/**
 * 解决递归堆栈溢出问题[https://github.com/pfan123/Articles/issues/50]
 * 1. 采用尾调用优化, chrome对尾递归不支持,可能还出现溢出问题
 * 2. while循环,累计操作
 */

// test six 复制 function 函数
// const add10 = (data) => {
//   return data + 10
// }
// let obj1 = { age: 12};
// let obj2 = {
//   a: obj1,
//   b: () => {},
//   add: add10,
//   c: [1,2,5]
// };

// let obj3 = cloneDeep2(obj2)
