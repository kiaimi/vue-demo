function Woman() {}
Woman.prototype.name = 'Lisa';
let woman = new Woman();
woman.name = 'Juli';
woman.name; // Juli
delete woman.name;
woman.name; // Lisa

class Man {
  constructor() {}
  name = 'Jammy';

  printName() {
    console.log(`printName-${this.name}`);
  }
}
let man = new Man();
man.name = 'Loki';
man.name; // Loki
delete man.name;
man.name; // undefined
Man.prototype.sex = 1;
man.sex = 2;
man.sex; // 2
delete man.sex;
man.sex; // 1

woman.__proto__;
// =>
// {
//   name: 'Lisa',
//   constructor: f Woman()
// }
man.__proto__;
// =>
// {
//   constructor: class Man()
// }

class Zombie extends Man {
  constructor() {
    super();
  }
  // name = 'Ark'
}
let zombie = new Zombie();
zombie.__proto__;

// class Elve {
//   constructor(name) {
//     this.name = name
//   }
// }
// let elve = new Elve('Sif')
// elve.name = 'ark'
// elve.name
// delete elve.name
// elve.name
//
function Person(name) {
  this.name = name || '测试';
}
let person = new Person();
person.name = 'abc';
person.name;
delete person.name;
person.name;

// --------------------------------------

// class Person {
//   constructor(name) {
//     this.name = name;
//   }

//   printName() {
//     console.log(`printName-${this.name}`);
//   }

//   commonMethods() {
//     console.error('error');
//   }
// }

// class Student extends Person {
//   constructor(name, score) {
//     super(name);
//     this.score = score;
//   }
//   printScore() {
//     cnosole.log(`printScore-${this.score}`);
//   }
// }

// /*                                               */

// function Person(name) {
//   //  实例成员就是在构造函数内部，通过this添加的成员。
//   //  实例成员只能通过实例化的对象来访问。
//   this.name = name;
//   this.printName = () => {
//     console.log(`printName-${this.name}`);
//   };
// }
// // 在构造函数本身上添加的成员，只能通过构造函数来访问
// // Person.sex = 1 // 这作为静态变量

// // Person.prototype.printName = () => {
// //   console.log(`printName-${this.name}`)
// // }
// Person.prototype.commonMethods = () => {
//   console.error('error');
// };

// function Student(name, score) {
//   // call传入调用的this
//   Person.call(this, name);
//   this.score = score;
//   this.printScore = () => {
//     console.log(`printScore-${this.score}`);
//   };
// }

// // Student.prototype = new Person()
// // Student.prototype = Object.create(new Person())
// Student.prototype = Object.create(Person.prototype);
// Student.prototype.constructor = Student;

// // Student.prototype.printScore = () => {
// //     console.log(`printScore-${this.score}`)
// // }
