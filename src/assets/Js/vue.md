## **1. 写 React / Vue 项目时为什么要在列表组件中写 key，其作用是什么？**

> diff 算法

1.  不用`key`编译会报错 (不使用 ESlint)可以不写 key，如果 Vnode 内容相等可以`就地复用`
2.  使用 id 等特殊标识 Item，可以使列表非末尾追加插入数据，diff 算法需要把后续所有 Vnode 更新

```
ABCDE => AGBCDE
/* 不使用key */
// 使用遍历方法寻找新旧节点是否相同
/* 使用index作为key */
// B=>G|C=>B...     后续所有Vnode必须更新,相同key值的节点的信息不一致
/* 使用id作为key */
// G新增 BCDE不变    后续Vnode可以进行复用，提高性能
```

```javascript
// vue项目  src/core/vdom/patch.js  -488行
// 以下是为了阅读性进行格式化后的代码

// oldCh 是一个旧虚拟节点数组
if (isUndef(oldKeyToIdx)) {
  oldKeyToIdx = createKeyToOldIdx(oldCh, oldStartIdx, oldEndIdx);
}
if (isDef(newStartVnode.key)) {
  // map 方式获取
  idxInOld = oldKeyToIdx[newStartVnode.key];
} else {
  // 遍历方式获取
  idxInOld = findIdxInOld(newStartVnode, oldCh, oldStartIdx, oldEndIdx);
}

// ...
function createKeyToOldIdx(children, beginIdx, endIdx) {
  let i, key;
  const map = {};
  for (i = beginIdx; i <= endIdx; ++i) {
    key = children[i].key;
    if (isDef(key)) map[key] = i;
  }
  return map;
}

// ...
// sameVnode 是对比新旧节点是否相同的函数
function findIdxInOld(node, oldCh, start, end) {
  for (let i = start; i < end; i++) {
    const c = oldCh[i];

    if (isDef(c) && sameVnode(node, c)) return i;
  }
}
```

---

<br />
<br />

## **2.Vue 的响应式原理中 Object.defineProperty 有什么缺陷？**

1. Object.defineProperty 无法监控到数组下标的变化，导致通过数组下标添加元素，不能实时响应；
2. Object.defineProperty 只能劫持对象的属性，从而需要对每个对象，每个属性进行遍历，如果，属性是对象，还要深度遍历.Proxy 可以劫持整个对象，并返回一个新的对象。
3. Proxy 不仅可以代理对象，还可以代理数组。还可以代理动态增加的属性。
   _原理_

- `2.x`: Vue 在初始化数据时，会使用`Object.definedProperty`重新定义 data 中的所有属性，当页面使用对应属性时，首先会进行依赖收集(收集当前组件的`watcher`)如果属性发生变化会进行通知相关依赖进行更新操作(发布订阅)。
- `3.x`: 使用`Proxy`代替`Object.definedProperty`。 因为 Proxy 可以直接监听对象和数组的变化，并且有多达 13 中拦截方法。

_Proxy 只会代理对象的第一层_

> 判断当前的 Reflect.get 的返回值是否为 Object，如果是则再通过`reactive`方法做代理，这样就实现了深度观测。

## _监测数组的时候可能出发多次 get/set,那么如何防止_

<br />
<br />

## **3.实现 vfor(vue 指令)**

```javascript
  // 注册一个全局自定义指令 `v-focus`
  Vue.directive('focus', {
    // 当被绑定的元素插入到 DOM 中时……
    inserted: function (el) {
      // 聚焦元素
      el.focus()
    }
  })

  // 局部注册指令
  directives: {
    focus: {
      // 指令的定义
      inserted: function (el) {
        el.focus()
      }
    }
  }
```

钩子函数:

> - bind: 只调用一次，指令第一次绑定到元素时调用。在这里可以进行一次性的初始化设置。
> - inserted: 被绑定元素插入父节点时调用(仅保证父节点存在，但不一定已被插入文档中)
> - update: 所在组件的 VNode 更新时调用,_但是可能发生在其子 VNode 更新之前_,指令的值可能发生了改变，也可能没有。但是可以通过比较更新前后的值来忽略不必要的模板更新
> - componentUpdated: 指令所在组件的 VNode*及其子 VNode*全部更新后调用。
> - unbind: 只调用一次,指令与元素解绑时调用。
>
> > 钩子函数的参数:

> > `el`: 指令所绑定的元素,可以用来直接操作 DOM
> > `binding`: 一个对象,包含以下 property;
> >
> > > - `name`: 指令名。
> > > - `value`: 指令绑定值 (ex: `v-qq="1 + 1"` => 2)
> > > - `oldValue`: 指令前一个值,仅在`update`和`componentUpdated`钩子中可用。无论值是否改变都可用。
> > > - `expression`: 字符串形式的指令表达式 (ex: `v-qq="1 + 1"` => '1 + 1')
> > > - `arg`: 传给指令的参数,可选 (ex: `v-qq:foo` => foo)
> > > - `modifiers`: 一个包含修饰符的对象。 (ex: v-qq:foo.a.b => { a: true, b: true })
> > > - `vnode`: Vue 编译生成的虚拟节点。
> > > - `oldVNode`: 上个虚拟节点,仅在`update`和`componentUpdated`钩子中可用。

---

<br />
<br />

## **4.vue 的过滤器**

```javascript
  // 注册一个全局过滤器
  Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
  })

  // 局部注册过滤器
  filters: {
    capitalize: function(value) {
      if (!value) return ''
      value = value.toString()
      return value.charAt(0).toUpperCase() + value.slice(1)
    }
  }
```

---

<br />
<br />

## **5.vue 生命周期**

1. beforeCreate: 所有尚未进行
2. created: 进行初始化事件,进行数据的观测,可以看到在 created 的时候数据已经和 data 属性进行绑定(el 尚未拥有)
3. beforeMount:
   - 有 el 继续编译生命周期
   - 无 el 生命周期结束
4. mounted: 真实的 Dom 挂载成功，数据绑定到页面上
5. beforeUpdate: 数据发生改变，触发对应组件的重新渲染
6. update: 继续在 beforeUpdate 之后，避免在此更新数据，可能陷入无限循环(this.data = !this.data)
7. beforeDestroy: 在实例销毁前调用,尚可以调用实例
8. destroyed: 实例销毁之后调用，所有都会解绑，子实例也会销毁 **注意关闭页面中的定时器，事件总线等**

_一般接口请求放在哪个生命周期？_

> 接口请求一般放在`mounted`中，ssr 不支持`mounted`,放到`created`中

_组件生命周期调用顺序_

> 组件的调用顺序都是`先父后子`，渲染完成的顺序是`先子后父`
> 组件的销毁操作是`先夫后子`，销毁完成的顺序是`先子后父`

**加载渲染过程**
`父beforeCreate->父created->父beforeMount->子beforeCreate->子created->子beforMount->子mounted->父mounted`

**子组件更新过程**
`父beforeUpdate->子beforeUpdate->子updated->父updated`

**父组件更新过程**
`父beforeUpdate->父updated`

**销毁过程**
`父beforeDestroy->子beforeDestroy->子destroyed->父destroyed`

---

<br />
<br />

## **6.vuex 的五个核心属性是什么？**

- state
- getters
- mutations
- actions
- modules

**1. vuex 在组件中批量引入 state 状态**

使用 mapState 辅助函数，利用对象展开运算符将 state 混入 computed 对象中

```javascript
import { mapState } from 'vuex';
export default {
  computed: {
    ...mapState(['role', 'number'])
  }
};

// 同理 mapGetters 辅助函数
```

**2. vuex 从 state 派生一些状态出来，且多个组件使用它**

```javascript
const store = new Vuex.Store({
  state: {
    price: 1.2,
    number: 1,
    discount: 0.7
  },
  getters: {
    total: state => state.price * state.number,
    discountTotal: (state, getters) => getters.total * state.discount
  }
});

// 使用
computed: {
  total() {
    return this.$store.getters.total
  },
  discountTotal() {
    return this.$store.getters.discountTotal
  }
}
```

**3. 怎么通过 getter 来实现在组件内可以通过特定条件来获取 state 的状态？**

```javascript
// 参考初始实现柯里化函数
// f(a,b,c) => f(a)(b)(c)
// const test = (a) => (b) => (c) => a+b+c

...
getters: {
  getGoodsById: state => id => state.goodsArr.find(x => x.id === id)
}
...
```

**4. Vuex 修改 state 的值**

```javascript
// mutation 方法
// 1. 注册 mutations 方法
const store = new Vuex.Store({
  state: {
    number: 10
  },
  /*
  mutation必须为同步函数
   */
  mutations: {
    SET_NUMBER(state, adta) {
      state.number = data;
    }
  },
  actions: {
    UPDATE_NUMBER({ commit }, data) {
      new Promise({
        ...
      }).then({
        commit('SET_NUMBER', data);
      })
    }
  }
});

// 2. 调用 commit
this.$store.commit('SET_NUMBER', 99);

// action 方法
/*
可支持异步调用
 */
this.$store.dispatch('UPDATE_NUMBER', 88)

// 多次调用 封装
import { mapMutations, mapActions } from 'vuex'
methods: {
  ...mapMutations({
    setNumber: 'SET_NUMBER',
    ...
  }),
  ...mapActions({
    updateNumber: 'UPDATE_NUMBER',
    ...
  })
}
this.setNumber(99)
this.updateNumber(88)
```

**5. vuex 模块**

```javascript
// createNamespacedHelpers 命名使用辅助函数
getters(state, getters, rootState, rootGetters); // 可以访问到其他的vuex
mutation(state, data); // 没有rootState 无法访问到其他vuex
action({ dispatch, commit, getters, rootGetters }, data);
/**
 * 如果 ModuleA 和 ModuleB 的 mutation 命名相同，使用 this.$store.commit('XX') 同时触发
 * 解决方法 给 Module 导出对象增加 `namespaced: true,`
 * 带命名空间的方法需要在方法前追加`ModuleX/`
 *
 */
```

---

<br />
<br />

## **7.Vue 双向绑定的原理(2.x 和 3.x 的区别)**

> `Object.definedProperty()` / `proxy`

```javascript
function observer() {
  viewContent.innerHTML = obj.name;
  modelContent.value = obj.name;
}

/**
 * Vue 2.0 --> Object.definedProperty()
 * 缺点:
 *   1. 需要对原对象进行深拷贝
 *   2. 对于每个对象都需要先进行定义，如果不定义，则无法监听数据的变动。
 *   3. 这也是为什么Vue中的一些数据的方法不能直接使用以及对象的变动特殊处理的原因
 */
let data = { name: '' };

let newData = _.cloneDeep(data);

Object.definedProperty(obj, 'name', {
  get() {
    return newData.name;
  },
  set(val) {
    if (newData.name === val) return;
    newData.name = val;
    observer(); // 观察者
  }
});

/**
 * Vue 3.0 --> Proxy (代理器)
 */
let obj = {};
obj = new Proxy(obj, {
  get(target, propKey) {
    return target[propKey];
  },
  set(target, propKey, value) {
    target[propKey] = value;
    observer();
  }
});
```

---

<br />
<br />

## **8.diff 算法**

- `if (oldVnode === vnode)` 不做处理
- `if (oldVnode.text !== null && vnode.text !== null && oldVnode.text !== vnode.text)` 更新节点的文本调用 `Node.textContent = vnode.text`
- `if (oldCh && ch && oldCh !== ch)` 调用 `updateChildren` 比较子节点， diff 核心
- `else if (ch)` 新增节点，调用 `createEle(vnode)`, vnode.el 已经引用老的 dom 节点
- `else if (oldCh)` 删除旧节点

> key 作用
>
> > 不设置 key，newCh 和 oldCh 只会从头到尾开始比对，对数组头部插入数据，整个数组所渲染的 dom 节点均要更新
> > 设置 key，key 生成对象会在 `oldKeyToIdx` 进行比对

---

<br />
<br />

## **9.前端权限设计方案**

**1. 登录权限控制**

```javascript
export const routes = [
  {
    path: '/login', //登录页面
    name: 'Login',
    component: Login
  },
  {
    path: '/list', // 列表页
    name: 'List',
    component: List
  },
  {
    path: '/myCenter', // 个人中心
    name: 'MyCenter',
    component: MyCenter,
    meta: {
      need_login: true //需要登录
    }
  }
];
```

> 通过`router.beforeEach`可以轻松实现上述目标
> `to`获取即将访问的路由信息，从其中获取`need_login`的值可以判断是否需要登录。再从`vuex`拿出用户信息

```javascript
router.beforeEach((to, from, next) => {
  const { need_login = false } = to.meta;
  const { user_info } = store.state; //从vuex中获取用户的登录信息
  if (need_login && !user_info) {
    // 如果页面需要登录但用户没有登录跳到登录页面
    const next_page = to.name; // 配置路由时,每一条路由都要给name赋值
    next({
      name: 'Login',
      params: {
        redirect_page: next_page,
        ...from.params //如果跳转需要携带参数就把参数也传递过去
      }
    });
  } else {
    //不需要登录直接放行
    next();
  }
});
```

**2. 页面权限控制**

> 将路由分成静态路由和动态路由 [减少匹配次数]
> 读取`vuex`中的路由信息(用户权限页面)
> 每层都可能是页面使用扁平化再遍历生成完整的路由表

```javascript
// router.beforeEach...
// ...

const router = createRouter({
  //创建路由对象
  history: createWebHashHistory(),
  routes
});

//动态添加路由
if (store.state.user != null) {
  //从vuex中拿到用户信息
  //用户已经登录
  const { permission_list } = store.state.user; // 从用户信息中获取权限列表
  const allow_routes = dynamic_routes.filter(route => {
    //过滤允许访问的路由
    return permission_list.includes(route.name);
  });
  allow_routes.forEach(route => {
    // 将允许访问的路由动态添加到路由栈中
    router.addRoute(route);
  });
  /**
   * 嵌套页面
   */
  // router.addRoute('Tabs', {
  //   path: '/list',
  //   name: 'List',
  //   component: List
  // });
}
```

_扩展：切换用户身份_

```javascript
const router = useRouter(); // 获取路由实例
const logOut = () => {
  //登出函数
  //将整个路由栈清空
  const old_routes = router.getRoutes(); //获取所有路由信息
  old_routes.forEach(item => {
    const name = item.name; //获取路由名词
    router.removeRoute(name); //移除路由
  });
  //生成新的路由栈
  routes.forEach(route => {
    router.addRoute(route);
  });
  router.push({ name: 'Login' }); //跳转到登录页面
};
```

**3. 内容权限控制**

```javascript
import router from './router';
import store from './store';

const app = createApp(App); //创建vue的根实例

//vue指令
app.directive('permission', {
  mounted(el, binding, vnode) {
    const permission = binding.value; // 获取权限值
    const page_name = router.currentRoute.value.name; // 获取当前路由名称
    const have_permissions = store.state.permission_list[page_name] || ''; // 当前用户拥有的权限
    if (!have_permissions.includes(permission)) {
      el.parentElement.removeChild(el); //不拥有该权限移除dom元素
    }
  }
});
```

```vue
<template>
  <div>
    <button v-permission="'U'">修改</button>
    <button v-permission="'D'">删除</button>
  </div>
  <p>
    <button v-permission="'C'">发布需求</button>
  </p>

  <!--列表页-->
  <div v-permission="'R'">
    ...
  </div>
</template>
```

_扩展：动态导航_

```vue
<template>
  <div v-if="permission_list['HOME']">系统首页</div>
  <div v-if="permission_list['SALE']">
    <p>销售</p>
    <div v-if="permission_list['S_NEED']">需求</div>
    <div v-if="permission_list['S_RESOURCE']">资源</div>
  </div>
  <div v-if="permission_list['PURCHASE']">
    <p>采购</p>
    <div v-if="permission_list['P_NEED']">需求</div>
    <div v-if="permission_list['P_RESOURCE']">资源</div>
  </div>
</template>
```

---

<br />
<br />

## **10.Vue 模版编译原理**

简单说，Vue 的编译过程就是将`template`转化为`render`函数。会经历以下阶段:

- 生成 AST 树
- 优化
- codegen

  首先解析模板，生成`AST语法树`(一种用 JavaScript 对象的形式来描述整个模板)。使用大量的正则表达式对模板进行解析。遇到标签、文本的时候都会执行对应的狗子进行相关处理。
  Vue 的数据是响应式的，但其实模板中并不是所有的数据都是响应式的。有一些数据首次渲染后不会再变化，对应的 DOM 也不会变化。那么优化过程就是深度遍历 AST 树，按照相关条件对树节点进行标记。这些被标记的节点(静态节点)我们就可以`跳过他们的对比`，对运行时的模板其很大的优化作用。
  编译的最后一部将优化后的 AST 树转换为可执行的代码。

---

<br />
<br />

## **11.Vue2.x 组件通信有哪些方式？**

> 父子组件通信
>
> > 父->子`props`,子->父`$on、$emit`
> > 获取父子组件实例`$parent、$children` >>`ref`获取实例的方法调用组件的属性或者方法。
>
> 兄弟组件通信
>
> > `Event Bus`实现跨组件通信`Vue.prototype.$bus = new Vue` >>`Vuex`
>
> 跨级组件通信(祖孙组件)
>
> > `Vuex` >>`$attrs、$listeners`

---

<br />
<br />

## **12.你都做过哪些 Vue 的性能优化？**

**编码阶段**

- v-if 和 v-for 不能连用
- 如果需要使用 v-for 给每项元素绑定事件时使用事件代理(事件委托)
- key 尽量使用独立 Id 标识。
- 防抖、节流
- 第三方模块按需导入

**打包优化**

- 调试模式开启 sourceMap,线上环境关闭此功能
- 多线程打包
- 压缩代码

**服务器**

- 开启缓存

---

<br />
<br />

## **13.hash 路由和 history 路由实现原理说一下**

> location.hash 的值实际就是 URL 中#后面的东西。

history 实际采用了 HTML5 中提供的 API 来实现，主要有 history.pushState()和 history.replaceState()。
