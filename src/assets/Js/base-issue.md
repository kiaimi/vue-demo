## **1. url 输入流程**

> 1. URL 解析
>    > I. 地址解析(判断输入时合法 url 或搜索词)
>    > II. 强制或半强制使用 HSTS 使用 HTTPS 访问页面 `ex: 80->443`
>    > III. 检查缓存 web 服务器(强缓存和协调缓存) `ex:打开过百度,重新打开`
> 2. DNS 查询
>    `浏览器缓存` -> `操作系统缓存` -> `路由器缓存` -> `ISP DNS 缓存` -> `根域名服务器`
>    > - 浏览器缓存: 浏览器会按照一定的频率缓存 DNS 记录
>    > - 操作系统缓存: host 文件
>    > - 路由缓存: 右键网络连接中的 DNS 设置(`8.8.8.8\114.114.114.114`)
>    > - ISP DNS 缓存: 互联网服务提供商的 DNS `电信/联通...`
>    > - 根域名服务器: 递归查询。 `aa.com.cn:.cn -> .com.cm ->aa.com.cn`
> 3. 应用层客户端发送 HTTP 请求[遵循 TCP/IP 协议,记忆 U 字型] `三次握手`
> 4. 传输层 TCP 传输报文[在此调用 TCP 的三次握手]
> 5. 网络层 IP 协议查询 MAC 地址[IP 地址和 MAC 地址是对应，请求方和接收方不一定在同一个局域网，所以需要多次中转]
> 6. 数据到达数据链路层[通过物理层的比特流传送, 客户端请求阶段完成]
> 7. 服务器接收数据[将分段的数据包重新组成原来 HTTP 请求]
> 8. 浏览器开始处理数据信息并渲染页面[构造 DOM]
>    > 1. 根据 HTML 解析出 DOM 树
>    > 2. 根据 CSS 解析出 CSS 规则树
>    > 3. 结合 DOM 树和 CSS 规则树，生成渲染树
>    > 4. 根据渲染树计算每一个节点的信息
>    > 5. 根据计算好的信息绘制页面
> 9. 断开连接 `四次挥手`

---

<br />
<br />

## **2.缓存,强缓存和协调缓存**

1. 不存在该缓存结果和缓存标识,强制缓存失败，则直接向服务器发起请求(首次请求一致)
2. 存在该缓存结果和缓存标识，但该结果已失效，强制缓存失败则使用协商缓存(下方说明)
3. 存在该缓存结果和缓存表示，且该结果尚未失效，强制缓存生效，直接返回该结果

- 强制缓存: 向浏览器缓存查找该请求结果，并且根据该结果的缓存规则来决定是否使用该缓存结果的过程
  - Expires(缓存到期时间 Http/1.0 绝对值 时间点)
  - Cache-Control(Http/1.1 下方取值)[优先级这个更高]
    1. public: 所有内容都将被缓存(客户端和代理服务器都可以缓存)
    2. private: 所有内容只有客户端可以缓存(默认值)
    3. no-cache: 客户端缓存内容，但是是否使用缓存则需要经过协商缓存来验证决定
    4. no-store: 所有内容都不会被缓存，即不使用强制缓存也不适用协商缓存
    5. max-age=xxx(xxx 是秒数): 缓存内容将在 xxx 秒后失效
- 协调缓存: 强制缓存失效后,浏览器携带缓存表示向服务器发起请求,由服务器根据缓存表示决定是否使用缓存的过程。
  - 返回 304 该资源无更新,从浏览器缓存获取缓存结果 (协商缓存成功)
  - 返回 200 该资源已更新,携带数据 (协商缓存失败)[响应头部携带其他字段]
    1. Last-Modified/If-Modified-Since (时间点) [响应头部/请求头部] {服务端根据两个值大小判断是否采用协商缓存}
    2. Etag / If-None-Match (唯一标识) [响应头部/请求头部] {服务段根据两个值是否相同才采用协商缓存}

* from memory cache(内存中缓存)
  1. 快速读取: 将编译解析后的文件,直接存入该进程的内存中,占用该进程一定的内存资源，以方便下次运行使用
  2. 时效性: 一旦该进程关闭，则该进程内存则会清空
* from disk cache(硬盘总缓存): 直接将缓存写入硬盘文件中,读取缓存需要对该缓存存放的硬盘文件进行 I/O 操作，然后重新解析该缓存内容,速度较慢

  [浏览器一般在 js 和图片等文件解析执行后直接存入内存缓存中,那么当刷新页面时只需要直接从内存缓存中读取,而 css 文件则会存入硬盘文件中,所以每次渲染页面都需要从硬盘读取缓存]

---

<br />
<br />

## **3.介绍下 Set、Map、WeakSet 和 WeakMap 的区别？**

> Set
>
> > 可以认为没有重复值的数组，键值键名相同的 map,
> > 支持遍历
> >
> > ```javascript
> > let s = new Set();
> > s.add(1);
> > s.has(1);
> > s.delete(1);
> > ```

---

> WeakSet
>
> > 仅支持引用类型对象的 Set
> > 对引用类型是弱引用，若外部不再使用该对象，可能被垃圾回收机制回收对象 不可遍历
> >
> > ```javascript
> > let ws = new WeakSet();
> > ws.add(1); // TypeError: Invalid value used in weak set
> > ```

---

> Map
>
> > 可以认为键名不再限制为字符串的对象(Object)
> > 支持遍历
> >
> > ```javascript
> > let m = new Map();
> > m.set(x, y);
> > m.get(x);
> > m.has(x);
> > s.delete(1);
> > ```

---

> WeakMap
>
> > 仅支持引用类型对象作为键名的 Map
> > 不支持遍历

---

<br />
<br />

## **4.ES5/ES6 的继承除了写法以外还有什么区别？**

```javascript
// ES6
class Super6 {}
class Sub6 extends Super6 {}

const sub6 = new Sub6();

Sub6.__proto__ === Super6; // true

// ES5
function Super5() {}
function Sub5() {}

Sub5.prototype = new Super5();
Sub5.prototype.constructor = Sub5;

var sub5 = new Sub5();

Sub5.__proto__ === Super5; // false
Sub5.__proto__ === Function.prototype; // true
/*
Function.prototype === Function.__proto__;
=>
Sub5.__proto__ === Function.__proto__;
*/
```

> ES6 子类可以直接通过**proto**寻址到父类
> ES5 子类通过**proto**寻址到函数实现

```javascript
function MyES5Array() {
  Array.call(this, arguments);
}

// it's useless
const arrayES5 = new MyES5Array(3); // arrayES5: MyES5Array {}

class MyES6Array extends Array {}

// it's ok
const arrayES6 = new MyES6Array(3); // arrayES6: MyES6Array(3) []
```

> this 的生成顺序不同。
>
> > ES5 先生成子类实例，再调用父类的构造函数修饰子类实例
> > ES6 先生成父类实例，在调用子类的钩爪函数修饰父类实例，继承内置对象

---

<br />
<br />

## **5.有以下 3 个判断数组的方法，请分别介绍它们之间的区别和优劣**

> Object.prototype.toString.call() 、 instanceof 以及 Array.isArray()

```javascript
Object.prototype.toString.call(arr); // '[object Array]'
arr instanceof Array; // true
Array.isArray(arr); // true
```

> Object.prototype.toString.call(arr); 最稳定，同时运行速度最慢
> instanceof 无法判断 iframe 其他页面的数据类型
> Array.isArray ES5 之前无法实现，查询速度最快

**扩展**

- typeof: 一般被用于判断一个变量的类型,七种基本类型
  - 根据底层存储变量，其机器码低位 1~3 存储其类型信息
    - 000: 对象
    - 010: 浮点数
    - 100: 字符串
    - 110: 布尔
    - 1: 整数
    - null: 所有机器码都为 0,所以用 typeof null 是为 'object'
    - undefined: 用 -2^30 整数标识
- instanceof: 用来晋级判断 Object 是从属哪种 Object(根据原型链查找)
- Object.prototype.toString.call(xxx) 精确判断 '[object xxx]'

---

<br />
<br />

## **6.介绍下重绘和回流（Repaint & Reflow），以及如何进行优化**

> - 浏览器采用流式布局模型(`Flow Based Layout`)
> - 浏览器会把 HTML 解析成 DOM， 把 CSS 解析成 CSSOM, DOM 和 CSSOM 合并就产生了渲染树(Render Tree)
> - 有了 Render Tree， 我们就知道了所有节点的样式，然后计算他们在页面上的大小和位置，最后把节点绘制到页面上。
> - 由于浏览器使用流式布局，对 Render Tree 的计算通常只需遍历一次就可以完成，**但 table 及其内部元素除外，他们可能需要多次计算，通常要花 3 倍于通等元素的时间，这也是为什么要避免使用 table 布局的原因之一**

> > **重绘**:由于节点的几何属性发生改变或者由于样式发生改变而不会影响布局的，称为重绘。(ex: outline、visibility、color、background-color...)重绘的代价是高昂的，因为浏览器必须验证 DOM 树上其他节点元素的可见性
> > <br /><br /> **回流/重排**:回流是布局或者几何属性需要改变就称为回流。回流是影响浏览器性能的关键因素，因为其变化涉及到部分页面(或者整个页面)的布局更新。一个元素的回流可能导致了其所有子元素以及 DOM 中紧随其后的节点、祖先节点元素的随后回流。

**回流必定会发生重绘，重绘不一定会引发回流。**

优化

1. CSS

- 使用`transform`替代`top`
- 使用`visibility`替换`display: none`(ps:前者指挥引起重绘，后者引发回流)
- 避免使用`table`布局
- 尽可能在 DOM 树的最末端改变 class
- 避免设置多层内联样式，CSS 选择符从右往左匹配查找，避免节点层级过多。
- 将动画效果应用到`position`属性为`absolute/fixed`的元素上
- 避免使用 CSS 表达式
- 将频繁重绘或者回流的节点设置为图层
- CSS3 硬件加速(GPU 加速)

2. javascript

- 避免频繁操作样式
- 避免频繁操作 DOM
- 避免频繁读取会引发回流、重绘的属性
- 对具有复杂动画的元素使用绝对定位

---

<br />
<br />

## **7.改造下面的代码，使之输出 0 - 9，写出你能想到的所有解法**

> ```javascript
> for (var i = 0; i < 10; i++) {
>   setTimeout(() => {
>     console.log(i);
>   }, 1000);
> } // 10 * 10
> ```
>
> > var 变量具有变量提升和，作用域为整个函数；let 作用域为代码块
> > 闭包问题，声明了十个自执行函数
> > setTimeout 作为宏任务，所以在声明后才执行

```javascript
// 1. 修改i的声明
for (let i = 0; i < 10; i++) {
  setTimeout(() => {
    console.log(i);
  }, 1000);
}

// 2. 解决自执行函数，改成立刻执行函数
for (var i = 0; i < 10; i++) {
  setTimeout(
    (() => {
      console.warn(i);
    })(),
    1000
  );
} // Refused to evaluate a string as JavaScript because 'unsafe-eval' is not an allowed source of script in the following Content Security Policy directive: "script-src <URL> <URL> 'self' 'unsafe-inline' https:".

// 3. setTimeout第三个参数作为回调函数的第一个参数传入
for (var i = 0; i < 10; i++) {
  setTimeout(
    i => {
      console.log(i);
    },
    1000,
    i
  );
}

// 4. 封装函数,将i 独立读取到函数内部
for (var i = 0; i < 10; i++) {
  (i => {
    setTimeout(() => {
      console.log(i);
    }, 1000);
  })(i);
}
```

---

<br />
<br />

## **8.下面的代码打印什么内容，为什么？ 怎么改造输出 10 和 20**

> ```javascript
> var b = 10;
> (function b() {
>   b = 20;
>   console.log(b);
> })();
> ```

```javascript
var b = 10;
(function b() {
  // 1. 内部作用域，先去找出已有变量b的声明，刚好函数b是具名函数;
  // 2. 此函数为IIFE函数，无法进行重新赋值(类似const的定义)
  // 3. 不带变量标识,会从作用域开始找改变量，如果当前作用域没有继续往上级作用域查找 [最后window声明该变量]
  b = 20;
  console.log(b); // [Function b]
})();
```

**(扩展)[https://segmentfault.com/q/1010000003951963]**

```javascript
/*
  1. 直接声明变量会初始化
  2. 函数声明会提升
  3. 变量赋值会覆盖原声明变量和函数
 */
console.log('1', a); // [Function a]
var a;
function a() {}
console.log('2', a); // [Function a]
// -----------
var b = 3;
function b() {}
console.log('3', b); // 10
// -----------
```

**追加题：如何让其分别打印出 10 和 20**

```javascript
// 10
var b = 10;
(function b() {
  b = 20; // 无视此语句即可
  console.log(window.b); // 读取window中的变量
})();

var b = 10;
(function b(b) {
  // b = 20; // 不做修改
  console.log(b); // 传参读取
})(b);

// 20
var b = 10;
(function b(b) {
  b = 20; // 作用域找到变量b修改
  console.log(b); // 传参读取
})(b);

var b = 10;
(function b() {
  var b = 20; // 独立声明变量和赋值
  console.log(b);
})();
```

---

<br />
<br />

## **9.下面代码中 a 在什么情况下会打印 1**

> > _考察点：js 的隐式类型转换_
>
> ```javascript
> var a = ?;
> if(a == 1 && a == 2 && a == 3){
>   console.log(1);
> }
> ```

---

`==`的五大规则

1. `NaN`和其他任何类型比较永远返回`false`(包括其自身)
2. Boolean 和其他任何类型比较，Boolean 首先被转换为 Number 类型。
3. String 和 Number 比较，先将 String 转换为 Number 类型(和`+`相反)
4. `Null == undefined`比较结果是 true，除此之外，null、undefined 和其他任何结果比较值都为 false
5. `原始类型`和`引用类型`做比较时,引用类型会依照`Toprimitive`规则转换为原始类型。

---

> 利用 toString

```javascript
let a = {
  i: 1,
  toString() {
    return a.i++;
  }
};
```

> 利用 valueOf

```javascript
let a = {
  i: 1,
  valueOf() {
    return a.i++;
  }
};
```

> 利用 Array 中转换先用 arr.join()转换成字符串，重写成弹出第一个值 arr.shift()，两者不需要传参

```javascript
let a = [1, 2, 3];
a.join = a.shift;
```

---

<br />
<br />

## **9.下面代码输出什么**

> ```javascript
> var a = 10;
> (function() {
>   console.log(a); // undefined
>   a = 5;
>   console.log(window.a); // 10
>   var a = 20;
>   console.log(a); // 20
> })();
> ```

_解析_

```javascript
var a = 10;
(function() {
  // 1. 目前所示的a应为window.a; 可是下面有 `var a = 20;`，上面说过变量声明会提升 此处应为 `var a;`
  // 所以第一个输出应为 undefined
  console.log(a); // undefined
  // 因为此处内部作用域声明了变量，更新内部变量
  a = 5;
  // 此处若输出`console.log(a); // 5`
  // 输出window的变量可以跳过内部函数，直接输出10
  console.log(window.a); // 10
  // 此处变量声明已提升 直接赋值20
  var a = 20;
  console.log(a); // 20
})();
```

---

<br />
<br />

## **10.原型和原型链**

- 原型: 每一个 JavaScript 对象(null 除外)在创建的时候就会与之关联另一个对象，这个对象就是我们所说的原型，每一个对象都会从原型"继承"属性。
- 原型链: 新建对象根据原型所继承的上级原型，拥有其的实现方法(除了重写方法)，原型链最后都指向 null

```
1. Person.prototype.constructor == Person // **准则 1：原型对象（即 Person.prototype）的 constructor 指向构造函数本身**
2. person01.__proto__ == Person.prototype // **准则 2：实例（即 person01）的**proto**和原型对象指向同一个地方**
```

---

<br />
<br />

## **11.输出以下代码的执行结果并解释为什么**

> ```javascript
> var a = { n: 1 };
> var b = a;
> a.x = a = { n: 2 };
> console.log(a.x); // undefined
> console.log(b.x); // {n: 2}
> ```

```javascript
var a = { n: 1 }; // *a = A1;
var b = a; // *b = *a = A1;
/*
连续赋值一般从右到左，可是`.`运算优先级更高
近似看成下方
 */
// a.x = {n: 2}; // A1内存扩出位置
// a = {n: 2}; // *a = A2;

// 可是A1扩展的A2区域被*a引用
a = b.x = { n: 2 };

console.log(a.x); // A2内存没有x变量
console.log(b.x); // A1内存查找x变量
a === b.x; // true
```

---

<br />
<br />

## **12.要求设计 LazyMan 类，实现以下功能。**

> ```javascript
> LazyMan('Tony');
> // Hi I am Tony
>
> LazyMan('Tony')
>   .sleep(10)
>   .eat('lunch');
> // Hi I am Tony
> // 等待了10秒...
> // I am eating lunch
>
> LazyMan('Tony')
>   .eat('lunch')
>   .sleep(10)
>   .eat('dinner');
> // Hi I am Tony
> // I am eating lunch
> // 等待了10秒...
> // I am eating diner
>
> LazyMan('Tony')
>   .eat('lunch')
>   .eat('dinner')
>   .sleepFirst(5)
>   .sleep(10)
>   .eat('junk food');
> // Hi I am Tony
> // 等待了5秒...
> // I am eating lunch
> // I am eating dinner
> // 等待了10秒...
> // I am eating junk food
> ```

```javascript
class LazyManClass {
  constructor(name) {
    this.name = name;
    console.log(`Hi I am ${this.name}`);
    /*
    开始读取任务列表并执行，由于使用宏任务，会先注入所有任务
     */
    setTimeout(() => {
      this.next();
    }, 0);
  }

  taskList = [];

  eat(food) {
    // const that = this;
    // const fn = (function(f) {
    //   return function() {
    //     console.log(`I am eating ${f}`);
    //     return that.next();
    //   };
    // })(food);
    const fn = () => {
      console.log(`I am eating ${f}`);
      this.next();
    };
    this.taskList.push(fn);
    return this; // 保持链式条件
  }

  sleepFirst(time) {
    // const that = this;
    // const fn = (function(t) {
    //   return function() {
    //     setTimeout(() => {
    //       console.log(`等待了${t}秒`);
    //       return that.next();
    //     }, t * 1000);
    //   };
    // })(time);
    const fn = () => {
      setTimeout(() => {
        console.log(`等待了${time}秒`);
        return that.next();
      }, t * 1000);
    };
    this.taskList.unshift(fn);
    return this;
  }

  sleep(time) {
    // const that = this;
    // const fn = (function(t) {
    //   return function() {
    //     setTimeout(() => {
    //       console.log(`等待了${t}秒`);
    //       return that.next();
    //     }, t * 1000);
    //   };
    // })(time);
    const fn = () => {
      setTimeout(() => {
        console.log(`等待了${time}秒`);
        return that.next();
      }, t * 1000);
    };
    this.taskList.push(fn);
    return this;
  }

  next() {
    const fn = this.taskList.shift();
    fn && fn();
  }
}

function LazyMan(name) {
  return new LazyManClass(name);
}
```

---

<br />
<br />

## **13.箭头函数与普通函数（function）的区别是什么？构造函数（function）可以使用 new 生成实例，那么箭头函数可以吗？为什么？**

1. 箭头函数没有 this，它会从自己的作用域链的上一层继承 this(因此无法使用 apply/call/bind 进行绑定 this 值)；
2. 不绑定 arguments，当在箭头函数中调用 arguments 时同样会向作用域链中查询结果；
3. 不绑定 super 和 new.target;
4. 没有 prototype 属性，即指向 undefined；
5. 无法使用 new 实例化对象，因为普通构造函数通过 new 实例化对象时 this 指向实例对象，而箭头函数没有 this 值，同时箭头函数也没有 prototype

---

<br />
<br />

## **14.['1', '2', '3'].map(parseInt) what & why ?**

```javascript
['1', '2', '3'].map(parseInt); // [1, NaN, NaN]

/**
 * string  要被解析的值
 * redix   字符串的基数 2-36
 * return  NaN/整数
 */
parseInt(string, radix);
/*
如果 radix 是 undefined、0或未指定的，JavaScript会假定以下情况：

如果输入的 string以 "0x"或 "0x"（一个0，后面是小写或大写的X）开头，那么radix被假定为16
字符串的其余部分被当做十六进制数去解析。

如果输入的 string以 "0"（0）开头， radix被假定为8（八进制）或10（十进制）。
具体选择哪一个radix取决于实现。ECMAScript 5 澄清了应该使用 10 (十进制)，但不是所有的浏览器都支持。因此，在使用 parseInt 时，一定要指定一个 radix。

如果输入的 string 以任何其他值开头， radix 是 10 (十进制)。
如果第一个字符不能转换为数字，parseInt会返回 NaN。
  */

map((current, index) => parseInt(current, index));
```

---

<br />
<br />

## **15.闭包调用(this 指向, 返回值等等)**

- 闭包: 是指那些能够访问自由变量[在函数里面使用，但既不是函数参数也不是函数的局部变量的变量]的函数，并且创建它的上下文已经销毁
  this-> windows/undefined(严格模式)

  > 外层函数嵌套内层函数
  > 内层函数引用外层函数的变量
  > 外层函数返回内层函数

  函数声明时会生成一个包含该函数作用域内部变量的活动对象，执行函数后函数作用域也会销毁，可是内部变量被内部的函数调用， js 的垃圾回收机制判断变量被使用不会销毁，所以生成一个只有内部函数使用的不会销毁变量

---

<br />
<br />

## **16.Es6 常用语法**

1. let 和 const
2. set 和 解构
3. 箭头函数
4. class
5. promise
6. 部分字符串和数组的新增方法
7. map 和 set

---

<br />
<br />

## **17.深拷贝和浅拷贝(只针对引用数据类型)**

- 深拷贝: 创建一个一摸一样的对象，新对象和原对象不共存内存，修改新对象不会改到原对象。
- 浅拷贝: 只复制某个对象的指针，而不复制对象本身，新旧对象还是共享一块内存。
- 赋值: 将底层的指针复制，基本数据类型更改会同步更改

实现方法:

- 深拷贝:
  > 1.  JSON.parse(JSON.stringify()) [此方法不能解析出函数]
  > 2.  手写递归方法
  > 3.  lodash \_.cloneDeep
- 浅拷贝:
  > 1.  Object.assign({}, xx)
  > 2.  Array.prototype.concat() [数组专用]
  > 3.  Array.prototype.slice() [数组专用][ ] class 应用

---

<br />
<br />

## **18.call，apply，bind 的区别**

```javascript
// 手撕代码
/**
 * 1. 获取this指向 (若为空指向根)
 * 2. 挂载方法到this上面
 * 3. 返回方法的函数
 * 4. this移除方法
 * // node 等没有 window 严格模式更加
 * @param  {[type]} obj [description]
 * @param  {Array}  arg [初始化为空数组，可以不去考虑null等值]
 * @return {[type]}     [description]
 */
Function.prototype.apply1 = function(
  obj = typeof windows === 'undefined' ? global : window,
  arg = []
) {
  obj.fn = this;
  let result = null;
  // undefined 和 null 无法解构
  result = obj.fn(...arg);
  delete obj.fn;
  return result;
};
/**
 * 同上
 * 传入参数是解构后，上是数组包含参数
 * @param  {[type]}    obj [description]
 * @param  {...[type]} arg [description]
 * @return {[type]}        [description]
 */
Function.prototype.call1 = function(
  obj = typeof windows === 'undefined' ? global : window,
  ...arg
) {
  obj.fn = this;
  let result = null;
  result = obj.fn(...arg);
  delete obj.fn;
  return result;
};

/**
 * 实际柯里化思想 闭包
 * @param  {[type]}    obj [description]
 * @param  {...[type]} arg [description]
 * @return {[type]}        [description]
 */
Function.prototype.bind1 = function(
  obj = typeof windows === 'undefined' ? global : window,
  ...arg
) {
  return (...innerArgs) => {
    this.call(obj, ...arg.concat(innerArgs));
  };
};

let test = {
  num: 99,
  add: function(a, b) {
    console.warn(a + b + this.num);
    console.log('-------');
    return a + b + this.num;
  }
};

let add = test.add;

setTimeout(function() {
  /*
  必须一次性传入所有参数
  接受两个参数
  第一个参数是this指向 null/undefined => windows
  第二个参数是原函数的参数值,以数组形象传入
   */
  add.apply(test, [10, 1]);

  /*
  必须一次性传入所有参数
  接受 n+1 个参数
  第一个参数是this指向 null/undefined => windows
  后续参数分别作为原函数的参数传入
   */
  add.call(test, 10, 1);

  /*
  可以分量传入参数
  接受 n+1 个参数
  第一个参数是this指向 null/undefined => windows
  后续参数分别作为原函数的参数传入
   */
  let addLast = add.bind(test, 10);
  addLast(1);
});
```

---

<br />
<br />

## **19.session, cookie, sessionstorage, loaclstorage 的区别**

| 类型    | 位置                                               | 限制                                        | 安全性                   |
| ------- | -------------------------------------------------- | ------------------------------------------- | ------------------------ |
| session | 服务器                                             | 一个 sessionId 只能匹配一个用户信息         | 较高                     |
| cookie  | 浏览器[未设置过期时间在内存，设置过期时间则在硬盘] | 不可跨域[即同一个域名可以使用同一个 cookie] | 不安全，因为可以随意修改 |

<!-- 存储在浏览器中 -->

| 类型           | 生命周期                 | 大小      | 与服务器通信           | 易用性                            |
| -------------- | ------------------------ | --------- | ---------------------- | --------------------------------- |
| cookie         | 定时或浏览器关闭         | 4k 左右   | 每次 http 请求携带头部 | 需要自己封装 setCookie、getCookie |
| sessionstorage | 页面会话期间存在         | 5M 或以上 | 可不参与               | 可以用原生                        |
| loaclstorage   | 除非被清理，否者一直存在 | 5M 或以上 | 可不参与               | 可以用原生                        |

---

<br />
<br />

## **20.代理(Proxy) 和反射(Reflect)**

> 元编程:這類電腦程式編寫或者操縱其它程式（或者自身）作為它們的資料，或者在執行時完成部分本應在編譯時完成的工作。
>
> > 代理: 重写原型的方法
> > 反射: 调用原型的方法

```javascript
let handler = {
  get: function(target, name) {
    console.log('target', target);
    console.log('name', name);
    if (target.hasOwnProperty(name)) {
      return target[name];
    } else {
      console.warn('err,找不到属性');
    }
    console.log('---------------');
  },
  set: function(target, property, value, receiver) {
    // 原初始目标对象 无法覆盖
    console.warn('target', target);
    console.warn('property', property);
    console.warn('value', value);
    console.warn('receiver', receiver);
    target[name] = value;
    console.log('---------------');
  },
  deleteProperty(target, property) {
    console.log(target, '删除属性', name);
    delete target[name];
  }
};
let p = new Proxy({}, handler);
p.a = 1;

console.log(p.a);
```

---

<br />
<br />

## **21.get 和 post 的区别**

| 类型 | 请求数据     | 缓存     | 安全性           | TCP 数据包 |
| ---- | ------------ | -------- | ---------------- | ---------- |
| GET  | URL 参数     | 自动     | 暴露在 URL 上 弱 | 一个       |
| POST | request body | 手动设置 | 请求体内 高      | 两个       |

_而对于 POST，浏览器先发送 header，服务器响应 100 continue，浏览器再发送 data，服务器响应 200 ok（返回数据）。_

---

<br />
<br />

## **22.`new` 操作发生了什么**

1. Js 内部生成一个对象
2. 将函数中的 this 指向该对象
3. 执行构造函数中的语句
4. 最终返回该对象实例

---

<br />
<br />

## **23.git 在另一个分支上提交某个 commit**

> 少量使用 cherry 将 commit 复制过去
>
> 一系列 commit 可以使用 rebase(注意分叉点)

---

<br />
<br />

## **24.css 动画的实现 transition 和 animation 搭配 @keyframes**

- transition: 过渡效果属性名 过渡时间 过渡效果速度曲线 效果开始的延迟时间(ex: transition: width 2s ease .5s)

> 事件触发时才会出现动画，是一次性的，不是自动开始，两个状态(初终)都需要具体的值，不能使用 auto none 之类的

- animation: 通过 animation-name 声明关键字 然后@keyframe 关键字 { // 定义状态 }

---

<br />
<br />

## **25.XSS 和 CSRF**

- XSS(ex:微信的拍一拍功能，备注改成'并喊了一声老公' **A 拍了一拍 B** => **A 拍了一拍 B 并喊了一声老公**，语义完全不同)

  1. 浏览器自带 HttpOnly (解决盗取 cookie 信息)
  2. 输入检查 输入文案判断有没有`<`、`>`、`script`等特殊字符
  3. 服务器输出也可以检查一番

> `http://xxx/search?keyword="><script>alert('XSS');</script>`
>
> > html 转义
> > `<a href="<%= escapeHTML(getParameter("redirect_to")) %>">跳转...</a>`
> > 白名单(增加校验判断)

XSS 分类

1. 存储型 XSS **常见论坛发帖、商品评论、用户私信等**
1. 攻击者将恶意代码提交到目标网络的数据库中。
1. 用户打开目标网站时，网站服务器端将恶意代码从数据库取出，拼接在 HTML 中返回给浏览器。
1. 用户浏览器接收到相应后解析执行，混在其中的恶意代码也被执行。
1. 恶意代码窃取用户数据并发送到攻击者的网站，或冒充用户的行为，调用目标网站接口执行攻击者指定的操作。
1. 反射型 XSS **常见通过 URL 传递参数的功能，ex:网站搜索、跳转等**
1. 攻击者构造出特殊的 URL,其中包含了恶意代码。
1. 用户打开带有恶意代码的 URL 时，网站服务端将恶意代码从 URL 取出，拼接在 HTML 中返回给浏览器。
1. 用户浏览器接收到相应后解析执行，混在其中的恶意代码也被执行。
1. 恶意代码窃取用户数据并发送到攻击者的网站，或冒充用户的行为，调用目标网站接口执行攻击者指定的操作。
1. DOM 型 XSS **DOM 型攻击中，取出和执行恶意代码都是浏览器端完成，属于前端 JavaScript 自身的安全漏洞**
1. 攻击者构造出特殊的 URL,其中包含了恶意代码。
1. 用户打开带有恶意代码的 URL。
1. 用户浏览器接收到相应后解析执行，前端 JavaScript 取出 Url 中的恶意代码并执行。
1. 恶意代码窃取用户数据并发送到攻击者的网站，或冒充用户的行为，调用目标网站接口执行攻击者指定的操作。

- CSRF(跨站请求伪造)
  1. 验证码
  2. 请求头的 Referer 域名地址是否相同

> 一个典型的 CSRF 攻击者有着如下的流程：
>
> - 受害者登录 a.com，并保留了登录凭证(cookie)
> - 攻击者引诱受害者访问了 b.com
> - b.com 向 a.com 发送了一个请求: a.com/act=xx。浏览器会默认携带 a.com 的 cookie
> - a.com 接收到请求后，对请求进行验证，并确认是受害者的凭证，误以为是受害者自己发送的请求。
> - a.com 以受害者的名义执行了 act=xxx
> - 攻击完成，攻击者在受害者不知情的情况下，冒充受害者，让 a.com 执行了自己定义的操作。

CSRF 类型

1. GET 类型的 CSRF
   `<img src=http://wooyun.org/csrf.php?xx=11 />`
2. POST 类型的 CSRF
   打开 b 页面自动提交表单到 a 地址(个人网站、博客)
3. 链接类型的 CSRF
   使用 a 标签(通常在图片嵌入恶意链接)

CSRF 特点

- 攻击一般发起在第三方网站，而不是被攻击的网站。被攻击的网站无法仿制攻击发生。
- 攻击利用受害者在被攻击网站的登录凭证，冒充受害者提交操作，而不是直接窃取数据。
- 整个过程攻击者并不能获取到受害者的登陆凭证，仅仅是"冒用"
- 跨站请求可以用各种方式：图片 URL、超链接、CORS、Form 提交等等

CSRF 防护

1. 同源检测
   `Origin Header`和`Referer Header`：
   1. 在 CSP 设置
   2. 页面头部增加 meta 标签
   3. a 标签增加 refererpolicy 属性
2. CSRF Token
3. 将 CSRF Token 输出到页面中(服务器传 token 过来，js 遍历页面 a 和 form 标签增加 token 标识)
4. 页面提交的请求携带 Token
5. 服务器验证 Token 是否正确
6. 双重 cookie 验证
7. 在用户访问网站页面时，想请求域名注入一个 Cookie，内容为随机字符串(例如`csrfcookie=vaklwhcjl`)
8. 在前端发起请求时，取出 cookie，添加到 URL 的参数中
9. 后端验证 URL 和 cookie 是否对应
10. Samesite Cookie 属性

---

<br />
<br />

## **26.跨域问题及处理**

_跨域是指一个域下的文档或脚本试图去请求另一个域下的资源，这里跨域是广义的。_

广义的跨域

```
1. 资源跳转: a链接、重定向、表单提交
2. 资源嵌入: <link>、<script>、<img>、<frame>等dom标签，还有样式中background:url()、@font-face()等文件外链
3. 脚本请求: js发起的ajax请求、dom和js对象跨域操作
```

解决方法：

1. 通过`jsonp`跨域
2. CORS
   > 服务端设置`Access-Control-Allow-Origin`即可
3. nginx 代理跨域
   > nginx 反向代理接口跨域

```shell
#proxy服务器
server {
    listen       81;
    server_name  www.domain1.com;

    location / {
        proxy_pass   http://www.domain2.com:8080;  #反向代理
        proxy_cookie_domain www.domain2.com www.domain1.com; #修改cookie里域名
        index  index.html index.htm;

        # 当用webpack-dev-server等中间件代理接口访问nignx时，此时无浏览器参与，故没有同源限制，下面的跨域配置可不启用
        add_header Access-Control-Allow-Origin http://www.domain1.com;  #当前端只跨域不带cookie时，可为*
        add_header Access-Control-Allow-Credentials true;
    }
}
```

---

<br />
<br />

## **27.HTTP 常用的状态码及使用场景？**

- 1xx: 表示目前是协议的中间状态，还需要后续请求
- 2xx: 表示请求成功
- 3xx: 表示重定向状态，需要重新请求
- 4xx: 表示请求报文错误
- 5xx: 服务器端错误

| code | info                                   |
| ---- | -------------------------------------- |
| 101  | 切换请求协议，从 HTTP 切换到 WebSocket |
| 200  | 请求成功，有响应体                     |
| 201  | 创建成功，一般 PUT 返回                |
| 301  | 永久重定向，会缓存                     |
| 302  | 临时重定向，不会缓存                   |
| 304  | 协商缓存命中                           |
| 403  | 服务器禁止访问                         |
| 404  | 资源未找到                             |
| 400  | 请求错误                               |
| 500  | 服务器端错误                           |
| 503  | 服务器繁忙                             |

---

<br />
<br />

## **28.浏览器事件机制**

**事件触发阶段**

- 捕获阶段:`window`往事件触发处传播，遇到注册的捕获会触发
- 传播到时间出发处事出发注册事件
- 冒泡阶段:从事件触发处往`window`传播，遇到注册的冒泡事件会触发

**事件监听器**

> 事件监听函数`element.addEventListener(<event-name>, <callback>, <use-capture>)`。在 element 这个对象上面添加一个时间监听器，当监听到有时间发生的时候，调用这个回调。至于这个参数，表示该事件监听是在"捕获"阶段中监听(设置为 true)还是在"冒泡"阶段监听(false)

**事件对象**

> 事件处理函数可以附加在各种对象上，包括 DOM 元素，window 对象等。当事件发生时，event 对象就会被创建并依次传递给事件监听器

**属性**

- `event.bubbles`: 是否会向 DOM 树上层元素冒泡。
- `event.cancelable`: 此属性说明该事件是否可以被取消默认行为，如果该事件可以用`prevent Default()`可以阻止与实践关联的默认行为，则返回`true`
- `event.currentTarget`: 当前事件绑定的对象
- `event.target`: 触发事件的对象引用
- `event.defaultprevented`: //
- `event.type`: 该事件对象的事件类型

**事件委托**

> 事件监听后，检测顺序就会从被绑定的 DOM 下滑到触发的元素，再冒泡会绑定的 DOM 上。事件监听器通过`event.target`获知到哪个子元素触发的。(常用于列表事件)[相对给每个 li 元素绑定事件，还不如直接给 ul 绑定一个元素]

---

<br />
<br />

## **29.什么是 HTML 语义化**

```
用正确的标签做正确的事情。
html语义化让页面内容结构化，结构更清晰，便于对浏览器、搜索引擎解析；
即使在没有样式CSS情况下也以一种文档格式显示，并且是容易阅读的；
使阅读源代码的人对网站更容易将网站分块，便于阅读维护理解。
```

_为什么标签要语义化？_

```
1. 标签语义化对于搜索引擎友好，便于SEO
2. 更容易让屏幕阅读器读出网页内容
3. 去掉或者丢失样式的时候，页面仍然能够呈现清晰的结构
4. 便于团队的开发和维护
```

---

<br />
<br />

## **30.HTTP 和 HTTPS 的区别**

- HTTP: 是互联网上应用最为广泛的一种网络协议，是一个客户端和服务器端请求和应答的标准(TCP),用于从 WWW 服务器传输超文本到本地浏览器的传输协议，它可以使浏览器更加高效，使网络传输减少。
- HTTPS: 是以安全为目的的 HTTP 通道，简单来说是 HTTP 的安全版，即 HTTP 下加入 SSL 层，HTTPS 的安全基础是 SSL，因此加密的详细内容就需要 SSL。

```
差异点：
  1. https协议需要申请ca证书，一般免费证书较少，因此需要一定的费用。
  2. http是超文本传输协议，信息是明文传输；https则是具有安全性的ssl加密传输协议。
  3. 端口号不同 80 443
  4.
```

---

<br />
<br />

## **31.三栏布局方法**

1. _浮动布局_
   > 对 SEO 搜索有影响，和渲染主要最后

```html
<div class="box">
  <div class="left"></div>
  <div class="right"></div>
  <div class="center"></div>
</div>
```

2. _flex_
   > flex 支持只能到 IE11

```html
<div class="box">
  <div class="left"></div>
  <div class="center"></div>
  <div class="right"></div>
</div>
```

3. _圣杯布局(`重点`)_
   > 三栏全部浮动
   >
   > > 左右栏配合相对位置方式移动到 box 的左右边上
   > > 左栏： margin-left:-100%; right: -leftWidth;
   > > 右栏： margin-right: -rightWidth;

```html
<div class="box clearfix">
  <div class="center fl"></div>
  <div class="left fl"></div>
  <div class="right fl"></div>
</div>
```

```css
.box {
  padding: 0 200px 0 150px;
}
.clearfix {
  clear: both;
}
.fl {
  float: left;
  position: relative;
}
.center {
  width: 100%;
}
.left {
  width: 150px;
  margin-left: -100%;
  left: -150px;
}
.right {
  width: 200px;
  margin-right: -200px;
}
```

4. _双飞翼布局(`重点`)_

```html
<div class="box clearfix">
  <div class="center fl">
    <div class="content"></div>
  </div>
  <div class="left fl"></div>
  <div class="right fl"></div>
</div>
```

```css
.box {
}
.clearfix {
  clear: both;
}
.fl {
  float: left;
  position: relative;
}
.center {
  width: 100%;
}
.center .content {
  margin: 0 200px 0 150px;
}
.left {
  width: 150px;
  margin-left: -100%;
}
.right {
  width: 200px;
  margin-left: -200px;
}
```

---

<br />
<br />

## **31.什么是 BFC**

> 块级格式化上下文，它是一个独立的渲染区域，其中的元素不受外界的影响，同样里面的元素也不会影响外面。

- BFC 的布局规则\*
- BFC 就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素。反之也如此。
- 内部的 Box 会在垂直方向，一个接一个的放置。
- Box 垂直方向的距离由 margin 决定；同一个 Box 中的相邻 Box 的 margin 会发生重叠。
- 每个 Box 的左边距从左往右格式化，即使浮动也是如此。
- BFC 的区域不会和 float box 重叠(两栏布局原理)。
- 计算 BFC 的高度时，浮动元素也参与计算。(清除浮动原理)。

_如何创建 BFC_

- float 的值不是 none；
- position 的 absolute 或 fixed；
- display 的值是 inline-block、table-cell、flex 或 inline-flex；
- overflow 不是 visible；

_BFC 特性_

1. 同一个 BFC 下外边距会发生折叠
2. BFC 可以用来清除浮动(overflow:hidden)
3. BFC 可以阻止元素被浮动元素覆盖

---

<br />
<br />

## **32.微任务、宏任务与 Event-Loop**

_宏任务_
| name | 浏览器 | node |
|---------------------|--------|------|
| `I/O` | ✅ | ✅ |
| `setTimeout` | ✅ | ✅ |
| `setInterval` | ✅ | ✅ |
| `setImmediate` | ❌ | ✅ |
| `reqAnimationFrame` | ✅ | ❌ |
| `script` | ✅ | ✅ |

_微任务_
| name | 浏览器 | node |
|--------------------|--------|------|
| `process.nextTick` | ❌ | ✅ |
| `MutationObserver` | ✅ | ❌ |
| `Promise` | ✅ | ✅ |
