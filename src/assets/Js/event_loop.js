// ex:
setTimeout(() => {
  console.log(2);
}, 0);

new Promise(resolve => {
  console.log(3);
  resolve();
  console.log(4);
}).then(() => {
  console.log(5);
});

console.log(8);

/**
 * 3
 * 4
 * 8
 * 5
 * 2
 */

/**
 * 宏任务: Script(整体代码)、setTimeout、setInterval、setImmediate、I/O、 UI rendering
 * 微任务: promise
 * 任务优先级： process.nextTick > promise.then > setTimeout > setImmediate
 */

console.log('start');

setTimeout(() => {
  console.log('timeout1');
}, 1); // 0 / 500

let p = new Promise(resolve => {
  console.log(`promise1`);
  resolve();
  console.log('promise2');
});

setTimeout(() => {
  console.log(`timeout2`);
}, 0);

p.then(() => {
  setTimeout(() => {
    console.log('timeout3');
  }, 0);
  console.log(`resolve1`);
});

setTimeout(() => {
  console.log(`timeout4`);
}, 0);
console.log('end');

// start
// promise1
// promise2
// end
// resolve1
// timeout2
// timeout4
// timeout3
// timeout1
/**
 * mac版chrome输出不一致，有空需确定
 */
