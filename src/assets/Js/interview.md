[TOC]

# 前端基础

## HTTP/HTML/浏览器

### <font color="2d8fd5">HTTP 和 HTTPS</font>

-   HTTP: 是互联网上应用最为广泛的一种网络协议，是一个客户端和服务器端请求和应答的标准(TCP),用于从 WWW 服务器传输超文本到本地浏览器的传输协议，它可以使浏览器更加高效，使网络传输减少。
-   HTTPS: 是以安全为目的的 HTTP 通道，简单来说是 HTTP 的安全版，即 HTTP 下加入 SSL 层，HTTPS 的安全基础是 SSL，因此加密的详细内容就需要 SSL。

```
差异点：
  1. https协议需要申请ca证书，一般免费证书较少，因此需要一定的费用。
  2. http是超文本传输协议，信息是明文传输；https则是具有安全性的ssl加密传输协议。
  3. 端口号不同 80 443
```

---

<br />

### <font color="2d8fd5">TCP 三次握手，一句话概括</font>

**确认双方接受和发送能力是否正常**
![三次握手](https://img-blog.csdnimg.cn/20210412094421590.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80ODMzNzU2Ng==,size_16,color_FFFFFF,t_70)

---

<br />

### <font color="2d8fd5">TCP 和 UDP 的区别</font>

![TCP/IP模型](https://image.fundebug.com/2019-03-21-01.png)

-   UDP
    1.  面向无连接
    2.  有单播、多播、广播的功能
    3.  UDP 是面向报文的
    4.  不可靠性
-   TCP
    1.  建立 TCP 连接(三次握手和四次挥手)
    2.  TCP 是面向连接的
    3.  可靠，并且提供拥塞控制
    4.  提供全双工通信

|              | UDP                                     | TCP                                    |
| ------------ | --------------------------------------- | -------------------------------------- |
| 是否连接     | 无连接                                  | 面向连接                               |
| 是否可靠     | 不可靠传输，不使用流量控制和拥塞控制    | 可靠传输，使用流量控制和拥塞控制       |
| 连接数量个数 | 支持一对一、多对一、一对多交互通信      | 只能是一对一通信                       |
| 传输方式     | 面向报文                                | 面向字节流                             |
| 首部开销     | 首部开销小，仅 8 字节                   | 首部最小 20 字节，最大 60 字节         |
| 使用场景     | 适用实时应用(IP 电话、视频会议、直播等) | 适用于要求可靠传输的应用，例如文件传输 |

---

<br />

### <font color="2d8fd5">WebSocket 的实现和应用</font>

> WebSocket 是 H5 中的协议，支持持久连接。WebSocket 是基于 Http 协议的，多了两个属性

```
Upgrade:webSocket
Connection:Upgrade
```

应用：(应和 UDP 做区别，判断身份再推送)

-   实时交互
-   实时推送信息
-   ***

<br />

### <font color="2d8fd5">HTTP 请求的方式，HEAD 方式</font>

| 序号 | 方法    | 描述                                                                                                                                 |
| ---- | ------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| 1    | GET     | 请求指定的页面信息，并返回实体主体                                                                                                   |
| 2    | HEAD    | 类似 GET 请求，只不过返回的响应中没有具体的内容，用于获取报头                                                                        |
| 3    | POST    | 向指定资源提交数据进行处理请求(例如提交表单或者上传文件)。数据被包含再请求体中。POST 请求可能会导致新的资源的建立和/或已有的资源更改 |
| 4    | PUT     | 从客户端向服务器传送的数据取代指定的文档的内容。(和 POST 区分点在一个请求多次重复请求，最后结果是否一致)                             |
| 5    | DELETE  | 请求服务器删除指定的页面                                                                                                             |
| 6    | CONNECT |                                                                                                                                      |
| 7    | OPTIONS | 允许客户端查看服务器的性能。(CORS 会自动发送一个 OPTIONS)                                                                            |
| 8    | TRACE   | 回显服务器收到的请求，主要用于测试或诊断                                                                                             |

> HEAD 请求优点：
>
> 1. 只请求资源的首部
> 2. 检查超链接的有效性
> 3. 检查网页是否被修改

---

**<font size=5>扩展：get 和 post 的区别</font>**

| 类型 | 请求数据     | 缓存     | 安全性           | TCP 数据包 |
| ---- | ------------ | -------- | ---------------- | ---------- |
| GET  | URL 参数     | 自动     | 暴露在 URL 上 弱 | 一个       |
| POST | request body | 手动设置 | 请求体内 高      | 两个       |

_而对于 POST，浏览器先发送 header，服务器响应 100 continue，浏览器再发送 data，服务器响应 200 ok（返回数据）。_

---

<br />

### <font color="2d8fd5">一个图片 URL 访问后直接下载怎样实现？</font>

```javascript
downloadImage(imgSrc, name) {
	let image = new Image();
	// 解决跨域 Canvas 污染问题
	image.setAttribute("crossOrigin", "anonymous");
	image.onload = function () {
		let canvas = document.createElement("canvas");
		canvas.width = image.width;
		canvas.height = image.height;
		let context = canvas.getContext("2d");
		context.drawImage(image, 0, 0, image.width, image.height);
		let url = canvas.toDataURL("image/png");
		let a = document.createElement("a");
		let event = new MouseEvent("click");
		a.download = name || "photo";
		a.href = url;
		a.dispatchEvent(event);
	}
	image.src = imgSrc;
}
```

---

<br />

### <font color="2d8fd5">说一下 web Quality(无障碍)</font>

_能够被残障人士使用的网站才能称得上一个易用的(易访问的)网站_

使用`alt`属性，浏览器起码可以显示或读出有关图像的描述

---

<br />

### <font color="2d8fd5">几个很实用的 BOM 属性对象方法？</font>

> **location 对象**

| 属性     | 描述                                                           |
| -------- | -------------------------------------------------------------- |
| href     | 返回或设置当前文档的 URL                                       |
| search   | 返回 URL 中的查询字符串部分                                    |
| hash     | 返回 URL#后面的内容，如果没有#，返回空                         |
| host     | 返回 URL 中的域名部分                                          |
| hostname | 返回 URL 中的主域名部分                                        |
| pathname | 返回 URL 的域名后的部分                                        |
| port     | 返回 URL 中的端口部分                                          |
| protocol | 返回 URL 中的协议部分                                          |
| assign   | 设置当前文档的 URL                                             |
| replace  | 设置当前文档的 URL,并且在 history 对象的地址列表中移除这个 URL |
| reload   | 重载当前页面                                                   |

> **history 对象**

| 属性    | 描述                                     |
| ------- | ---------------------------------------- |
| go      | 前进或后退指定的页面数 `history.go(num)` |
| back    | 后退一页                                 |
| forward | 前进一页                                 |

> **navigator 对象**

| 属性          | 描述                           |
| ------------- | ------------------------------ |
| userAgent     | 返回用户代理头的字符串表示     |
| cookieEnabled | 返回浏览器是否支持(启用)cookie |

---

<br />

### <font color="2d8fd5">说一下 HTML5 drag api</font>

| 属性      | 描述                                                   |
| --------- | ------------------------------------------------------ |
| dragstart | 事件主体是被拖放元素，在开始拖放被拖放元素时触发       |
| drag      | 事件主体是被拖放元素，在正在拖放被拖放元素事触发       |
| dragenter | 事件主体是目标元素，在被退房元素进入某元素时触发       |
| dragover  | 事件主体是目标元素，在被拖放在某元素内移动时触发       |
| dragleave | 事件主体是目标元素，在被拖放元素移除目标元素时触发     |
| drop      | 事件主体是目标元素，在目标元素完全接受被拖放元素时触发 |
| dragend   | 事件主体时被拖放元素，在整个拖放操作结束时触发         |

---

<br />

### <font color="2d8fd5">说一下 Http 2.0</font>

![HTTP发展史](https://user-gold-cdn.xitu.io/2018/12/9/167926d367568b74?imageView2/0/w/1280/h/960/format/webp/ignore-error/1)

-   HTTP/0.9——单行协议[HTTP 只支持 GET 方法；没有首部；只能获取纯文本。]
-   HTTP/1.0——搭建协议的框架[HTTP 正式被作为标准公布，增加首部、状态码、权限、缓存、长连接(默认短链接)]
-   HTTP/1.1——进一步完善[默认了长连接；强制客户端提供`Host`首部；管线化；Cache-Control、ETag 等缓存的相关扩展]

> 管线化： 将多个 HTTP 请求整批提交技术，而在传送过程中不需先等待服务端回应。(仅支持 GET 和 HEAD 请求)

_短连接_|_长连接_|_管线化_
![HTTP连接模式](https://img-blog.csdnimg.cn/2020032007452929.png#pic_center)

---

<br />

### <font color="2d8fd5">补充 400 和 401、403 状态码</font>(后续整理状态码)

**PASS**

---

<br />

### <font color="2d8fd5">fetch 发送 2 次请求的原因</font>

> 发送两次仅在 post 请求，fetch 第一次发送是 Option 请求,用来询问服务器是否支持修改请求头，正常返回 204 状态码，第二次才真正发送 post 请求

---

<br />

### <font color="2d8fd5">Cookie、sessionStorage、localStorage 的区别</font>

| 类型    | 位置                                               | 限制                                        | 安全性                   |
| ------- | -------------------------------------------------- | ------------------------------------------- | ------------------------ |
| session | 服务器                                             | 一个 sessionId 只能匹配一个用户信息         | 较高                     |
| cookie  | 浏览器[未设置过期时间在内存，设置过期时间则在硬盘] | 不可跨域[即同一个域名可以使用同一个 cookie] | 不安全，因为可以随意修改 |

<!-- 存储在浏览器中 -->

| 类型           | 生命周期                 | 大小      | 与服务器通信           | 易用性                            |
| -------------- | ------------------------ | --------- | ---------------------- | --------------------------------- |
| cookie         | 定时或浏览器关闭         | 4k 左右   | 每次 http 请求携带头部 | 需要自己封装 setCookie、getCookie |
| sessionstorage | 页面会话期间存在         | 5M 或以上 | 可不参与               | 可以用原生                        |
| loaclstorage   | 除非被清理，否者一直存在 | 5M 或以上 | 可不参与               | 可以用原生                        |

---

<br />

### <font color="2d8fd5">说一下 web worker</font>

> JS 是单线程没有多线程，当 JS 在页面玉兴长耗时同步任务时会导致页面假死影响用户体验，从而需要设置把任务队列中。
> **Web Worker 使一个 Web 应用程序可以在与主执行线程分离的后台线程中运行一个脚本操作。**

限制：

1. 同源限制
   (worker 线程执行的脚本文件必须和主线程的脚本文件同源)
2. 文件限制
   (worker 线程无法读取本地文件，只能通过网络读取)
3. DOM 操作限制
   (worker 是独立于主线程，所以无法操作主线程的 window 有关的属性，例如 DOM 对象、`document`、`window`等对象；但可以获取到`navigator`、`location(只读)`、`XMLHttpRequest`、`setTimeout族`等浏览器 API)
4. 通信限制
   (因为两者不在同一个上下文，不能直接通信，需要通过`postMessage`发放来通信)
5. 脚本限制
   (worker 线程不能执行`alert`、`confirm`,但可以使用`XMLHttpRequest`对象发出 ajax 请求)

> 下表用 Worker.代替 window.worker.

```javascript
/**
 * @param {[type]} jsUrl   [脚本网站地址]
 * @param {[Object]} options [配置对象]
 */
window.worker = new Worker(jsUrl, options);
Worker.XXX;
self.YYY;
```

<center>*Worker属性表*</center>
|      属性      |                                      描述                                      |
|----------------|--------------------------------------------------------------------------------|
| onerror        | 指定error事件的监听函数                                                        |
| onmessage      | 指定message事件的监听函数，发送过来的数据在Event.data属性中                    |
| onmessageerror | 指定messageerror事件的监听函数。发送的数据无法序列化成字符串时，会触发这个事件 |
| postMessage    | 向Worker线程发送消息                                                           |
| terminate      | 立即终止Worker线程                                                             |

---

<center>*self属性表*</center>
|      属性      |                                    描述                                    |
|----------------|----------------------------------------------------------------------------|
| name           | Worker的名字，该属性只读，由构造函数指定                                   |
| onmessage      | 指定message事件的监听函数                                                  |
| onmessageerror | 指定messageerror事件的监听。发送的数据无法序列化成字符串时，会触发这个事件 |
| close          | 关闭Worker线程                                                             |
| postMessage    | 向产生这个Worker线程发送消息                                               |
| importScropts  | 加载JS脚本                                                                           |

---

<br />

### <font color="2d8fd5">对 HTML 语义化标签的理解</font>

优点：

-   标签语义化有助于构架良好的 HTML 结构，有利于搜索引擎的建立索引、抓取。
-   有利于不同设备的解析(屏幕阅读器、盲人阅读器等)
-   有利于构建清晰的结构，有利于团队的开发、维护

> 常用标签：
>
> -   `h1`~`h6`，作为标题使用
> -   `p`段落标记
> -   `ul`、`ol`、`li`
> -   `dl`、`dt`、`dd`、`dl`就是"定义列表"。比如词典里面的词的解析、定义就可以用这种列表。
> -   `em`、`strong`；`em`作为强调，`strong`作为重点强调
> -   `table`、`thead`、`tbody`、`td`、`th`、`captions` > _H5 新增的标签_
> -   `header`：页眉
> -   `footer`：页脚
> -   `nav`:导航栏
> -   `aside`:附属信息
> -   `section`:文章某节

---

<br />

### <font color="2d8fd5">iframe 是什么？有什么缺点？</font>

> iframe(文档中嵌入文档)当前页面加载其他页面的信息

缺点：

-   阻塞主页面的 onload 事件
-   和主页面共享连接池，影响页面并行加载
-   不利于搜索引擎优化
-   设备兼容性差
-   增加服务器的 HTTP 请求，对于大型网站是不可取的

---

<br />

### <font color="2d8fd5">代理(Proxy) 和反射(Reflect)</font>

> 元编程:這類電腦程式編寫或者操縱其它程式（或者自身）作為它們的資料，或者在執行時完成部分本應在編譯時完成的工作。
>
> > 代理: 重写原型的方法
> > 反射: 调用原型的方法

```javascript
let handler = {
	get: function (target, name) {
		console.log('target', target);
		console.log('name', name);
		if (target.hasOwnProperty(name)) {
			return target[name];
		} else {
			console.warn('err,找不到属性');
		}
		console.log('---------------');
	},
	set: function (target, property, value, receiver) {
		// 原初始目标对象 无法覆盖
		console.warn('target', target);
		console.warn('property', property);
		console.warn('value', value);
		console.warn('receiver', receiver);
		target[name] = value;
		console.log('---------------');
	},
	deleteProperty(target, property) {
		console.log(target, '删除属性', name);
		delete target[name];
	}
};
let p = new Proxy({}, handler);
p.a = 1;

console.log(p.a);
```
