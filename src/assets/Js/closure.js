var data = [];

for (var i = 0; i < 3; i++) {
  data[i] = function() {
    console.log(i);
  };
}

data[0]();
data[1]();
data[2]();
// ===>

/**
 * i声明在全局, 输出方法体内部没有声明该变量, 所以查找到外部
 * 外部的i执行循环后一直处于i=3状态
 */

// var data = [];
// var i;
// for (i = 0; i < 3; i++) {
//   data[i] = function() {
//     console.log(i);
//   };
// }
// // 此时i = 3

// /*            解决               */
// /**
//  * 1.使用ES6中let变量
//  * 使全局变量变成块级变量
//  */
// var data = [];

// for (let i = 0; i < 3; i++) {
//   data[i] = function() {
//     console.log(i);
//   };
// }

// // 每次for循环执行 {
// //   let i = 0/1/2
// //   data[i] = function() {
// //     console.log(i);
// //   }
// // }

// /**
//  * 2.返回匿名函数赋值
//  * 在调用之前就将函数执行
//  */
// var data = [];

// for (var i = 0; i < 3; i++) {
//   data[i] = (function(num) {
//     return function() {
//       console.log(num);
//     };
//   })(i);
// }
