# **核心流程解析**

> At its core, **webpack** is a static module bundler for modern JavaScript applications.

<img src="https://mmbiz.qpic.cn/mmbiz_png/zPh0erYjkib2XsLZoPd59DHP96Bt3mjusYicujtcnkjdvLHTRHzgSqjJW1QLpNcPhbusgz4RjP7AYVqgRQRXMb8A/640?wx_fmt=png&tp=webp&wxfrom=5&wx_lazy=1&wx_co=1" width = "100%" height = "auto" />

_内容转换+资源合并_

1. 初始化阶段
1. **初始化参数**: 从配置文件、配置对象、Shell 参数中读取，与默认配置结合得出最终的参数。
1. **创建编译器对象**: 用上一步得到的参数创建`Compiler`对象。
1. **初始化编译环境**: 包括注入内置插件、注册各种模式工厂、初始化 RuleSet 集合、加载配置的插件等。
1. **开始编译**: 执行`compiler`对象的`run`方法
1. **确定入口**: 根据配置中的`entry`找出所有的入口文件，调用`compilition.addEntry`将入口文件转换为`dependence`对象。
1. 构建阶段
1. **编译模式(make)**: 根据`entry`对应的`dependence`创建`module`对象，调用`loader`将模块转译为标准 JS 内容，调用 JS 解析器将内容转换为 AST 对象，从中找出该模块依赖模块，再递归本步骤知道所有入口以来的文

+件都经过了本步骤的处理

2. **完成模块编译**: 上步递归处理所有能触达到的模块后，得到了每个模块被翻译后的内容以及他们之间的依赖关系图
3. 生成阶段
4. **输出资源(seal)**: 根据入口和模块之间的依赖关系，组装成一个个包含多个模块的`Chunk`，在把每个`Chunk`转换成一个单独的文件加到输出列表，这部是可以修改输出内容的最后机会。
5. **写入文件系统(emitAssets)**: 在确定好输出内容后，根据配置确定输出的路径和文件名，吧文件内容写入到文件系统

- `Entry`: 编译入口，webpack 编译的起点
- `Compiler`: 编译管理器，webpack 启动后会创建`compiler`对象，该对象一直存活直到结束退出
- `Comilation`: 单次编辑过程的管理器，比如`watch = true`时，运行过程中只有一个`compiler`但每次文件变更出发重新编译时，都会创建一个新的`compilation`对象
- `Dependence`: 依赖对象，webpack 基于该类型记录模块间以来关系。
- `Module`: webpack 内部所有资源都会以"`module`"对象形式存在，所有关于资源的操作、转义、合并都是以"module"为单位进行的。
- `Chunk`: 编译完成准备输出时，webpack 会将`module`按特定的规则组织成一个一个的`chunk`，这些`chunk`某种程度上跟最终输出一一对应。
- `Loader`: 资源内容转换器，其实就是实现从内容 A 转换 B 的转换器。
- `Plugin`: Webpack 构建过程中，会在特定的时机广播对应的事件，插件监听这些时间，在特定时间点介入编译过程。

## **1.有哪些常见的 Loader？**

- `file-loader`: 把文件输出到一个文件夹中，在代码中通过相对 URL 去引用输出的文件(处理图片和字体)
- `source-map-loader`: 加载额外的 Source Map 文件，以方便断点调试(调试和编译前代码对应)
- `babel-loader`: 把 ES6 转换成 ES5
- `ts-loader`: 将 TypeScript 转换成 JavaScript
- `sass-loader`: 将 SCSS/SASS 代码转换 CSS
- `css-loader`: 加载 CSS，支持模块化、压缩、文件导入等特性
- `style-loader`: 把 CSS 代码注入到 Javascript 中，通过 DOM 操作区加载 CSS
- `postcss-loader`: 扩展 CSS 语法，使用下一代 CSS，可以配合 autoprefixer 插件，自动补齐 CSS3 前缀
- `eslint-loader`: 通过 EsLint 检查 Javascript 代码
- `vue-loader`: 加载 Vue.js 单文件组件
- `cache-loader`: 可以在一些性能开销较大的 Loader 之前添加，目的是将结果缓存到磁盘里

---

## **2.有哪些常见的 Plugin？**

- `clean-webpack=plugin`: 目录清理
- `webpack-bundle-analyzer`: 可以查看项目模块大小进行按需优化
- `speed-measure-webpack-plugin`: 各个文件 Loader 加载速度显示，可以针对性优化打包

---

## **3.Loader 和 Plugin 的区别？**

`Loader`本质就是一个函数，在该函数中对接受到的内容进行转换，返回转换后的结果。因为 Webpack 只认识 JavaScript，所以 Loader 就成为了翻译官，对其他类型的资源进行转移的预处理工作。
在 module.rules 中配置，作为模块的解析规则，类型为数组。每一项都是一个 Object，内部包含了 test(类型文件)、loader、options(参数等属性)。

---

`Plugin`就是插件，基于事件流框架`Tapable`,插件可以扩展 Webpack 的功能，在 Webpack 运行大的生命周期中会广播出许多事件，Plugin 可以监听这些事件，在核实的时机通过 Webpack 提供 Api 改变输出结果。
在 plugin 中单独配置，类型为数组，每一项是一个 plugin 实例，参数都通过构造函数传入。

---
