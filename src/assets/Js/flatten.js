function flatten(arr) {
  var result = [];
  for (var i = 0, len = arr.length; i < len; i++) {
    if (Array.isArray(arr[i])) {
      result = result.concat(flatten(arr[i]));
    } else {
      result.push(arr[i]);
    }
  }
  return result;
}

const flatten2 = arr => {
  return [].concat(...arr.map(x => (Array.isArray(x) ? flatten(x) : x)));
};
