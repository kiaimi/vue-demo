```javascript
// 浅拷贝
/**
 * 1.判断初始值([]/{}/基本类型)
 * 2.判断每个属性所从
 */

// 深拷贝
/**
 * 1.判断初始值([]/{}/基本类型)
 * 2.判断每个属性所从(调用自身函数)
 */

// 深拷贝(内部属性同一个对象实现)
/**
 * 1.初始化hash表(WeakMap)
 * 2.判断source是否处于hash中
 * 3.判断初始值([]/{}/基本类型)
 * 4.判断每个属性所从(调用自身函数)
 * 5.set hash表
 */

// a.a = a // 对象属性是对象本身(递归可能堆栈爆)
// 深拷贝 循环遍历
/**
 * 1.初始化 WeakMap
 * 2.初始化根节点
 * 3.初始化循环列表
 * {
 *   parent: root // 对象的上级指向
 *   key: undefined // 对象的属性名
 *   data：source // 对象的值
 * }
 * 4.执行循环
 * 4.1读取item的值
 * 4.2判断复制的位置对象
 * 4.3循环执行属性的值(判断是否对象或者数组，加入循环列表，加入到hash表)
 */

/*                                            */

// 柯里化
/**
 * 1.初始化变量(做空输入默认值)
 * 2.使用闭包(计算原函数需要长度)
 * 3.读取原传入参数+新的参数(并入或生成新数组)
 * 4.参数数量足够调用原函数
 * 5.参数不足够调用柯里化函数
 */

/*                                            */

// Promise
/**
 * 1.判断输入是否为函数
 * 2.初始值
 * 3.执行输入函数(try/catch)
 * 4.执行成功回调和执行失败回调
 * 5.实现then的回调
 * 5.1参数是否为函数
 * 5.2根据状态执行对应回调
 */
```

## **深拷贝和浅拷贝**

```javascript
/**
 * 创建指定深度和广度的对象
 * https://segmentfault.com/a/1190000016672263
 * @param  {[type]} deep    [description]
 * @param  {[type]} breadth [description]
 * @return {[type]}         [description]
 */
function createData(deep, breadth) {
  var data = {};
  var temp = data;

  for (var i = 0; i < deep; i++) {
    temp = temp['data'] = {};
    for (var j = 0; j < breadth; j++) {
      temp[j] = j;
    }
  }

  return data;
}

/**
 * 暂不考虑基本类型转使用 new 换成引用类型，基本类型的原型链没有更改原数据的方法
 */

const cloneShallow = source => {
  /*
  原备选选择类型有
  1. typeof => 使用 new 方法新建的基本类型都是'object'
  2. instanceof
   */
  const type = Object.prototype.toString.call(source);
  switch (type) {
    case '[object Array]':
      return cloneShallowOther(source, 'array');
    case '[object Object]':
      return cloneShallowOther(source, 'object');
    default:
      return source;
  }
};

const cloneShallowOther = (source, type = 'object') => {
  let target = {};
  if (type === 'array') target = [];
  for (let key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      target[key] = source[key];
    }
  }
  return target;
};

const cloneDeep = source => {
  let target;
  const type = Object.prototype.toString.call(source);
  switch (type) {
    case '[object Array]':
      target = [];
      break;
    case '[object Object]':
      target = {};
      break;
    default:
      return source;
  }
  for (let key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      target[key] = cloneDeep(source[key]);
    }
  }
  return target;
};

/**
 * [原基础上加入hash表,保证引入类型的指向相同]
 * @param  {[type]}  source [description]
 * @param  {WeakMap} hash   [弱引用，垃圾回收机制执行]
 * @return {[type]}         [description]
 */
const cloneDeep2 = (source, hash = new WeakMap()) => {
  // 判断是否存在哈希表中,直接使用
  if (hash.has(source)) return hash.get(source);
  let target;
  const type = Object.prototype.toString.call(source);
  switch (type) {
    case '[object Array]':
      target = [];
      break;
    case '[object Object]':
      target = {};
      break;
    default:
      return source;
  }
  for (let key in source) {
    if (Object.prototype.hasOwnProperty.call(source, key)) {
      target[key] = cloneDeep2(source[key], hash);
    }
  }
  hash.set(source, target);
  return target;
};

/**
 * 判断是否引用类型 Array/
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
const isQuote = data => {
  const type = Object.prototype.toString.call(data);
  switch (type) {
    case '[object Array]':
      return true;
    case '[object Object]':
      return true;
    default:
      return false;
  }
};

/**
 * 初始化类型
 * @param  {[type]} data [description]
 * @return {[type]}      [description]
 */
const initTemp = data => {
  const type = Object.prototype.toString.call(data);
  switch (type) {
    case '[object Array]':
      return [];
    case '[object Object]':
      return {};
    default:
      throw new Error('类型不正确,此处应为引用类型Array/Object');
  }
};

/**
 * 使用遍历方法, 广度遍历
 * 无法实现 hash表 指针两个指向同一个对象
 * 已解决 指针同一个对象
 * @param  {[type]} source [description]
 * @return {[type]}        [description]
 */
const cloneDeep3 = source => {
  /* 初始化数据 */
  const hash = new WeakMap();
  if (!isQuote(source)) {
    return source;
  }
  let root = initTemp(source);
  const loopList = [
    {
      parent: root,
      key: undefined,
      data: source
    }
  ];

  while (loopList.length) {
    const node = loopList.pop();
    const parent = node.parent;
    const key = node.key;
    const data = node.data;

    let res = root;
    if (hash.has(data)) {
      res = parent[key] = hash.get(data);
    } else if (typeof key !== 'undefined') {
      // 非根节点
      res = parent[key] = initTemp(data);
    }

    for (let k in data) {
      if (isQuote(data[k])) {
        // 引用类型
        // 不考虑相同引用类型
        // loopList.push({
        //   parent: res,
        //   key: k,
        //   data: data[k]
        // });
        if (hash.has(data[k])) {
          res[k] = hash.get(data[k]);
        } else {
          loopList.push({
            parent: res,
            key: k,
            data: data[k]
          });
          hash.set(data[k], data[k]);
        }
      } else {
        // 普通类型
        res[k] = data[k];
      }
    }
  }
  return root;
};
```

## **柯里化**

```javascript
function add(a, b, c) {
  return a + b + c;
}
/**
 * 3.收集空洞位置，填充上去
 * f(_)(1)
 * f(1,_)(2)
 * f(1,_,3)(2)
 * f(1)(_)(2)
 * f(_,_)(2)(3)(1)
 */
const _ = Symbol('empty'); // {} // 一般不会传入参数使用Symbol 区别开options等于空对象尴尬
const checkLenth = (arr, length) => {
  const index = arr.findIndex(x => x === _);
  if (index === -1 && arr.length >= length) {
    return true;
  } else if (index > length) {
    return true;
  } else {
    return false;
  }
};
const curry = function(fn, args = []) {
  let length = fn.length;

  return function() {
    let _args = args.slice(0);
    let arr = Array.prototype.slice.call(arguments);

    /**
     * 这种写法会使 (1, _, 3) => (1,3)
     */
    // arr.forEach(item => {
    //   const index = _args.findIndex(x => x === _)
    //   if (index !== -1) {
    //     _args.splice(index, 1, item)
    //   } else {
    //     _args.push(item)
    //   }
    // })
    let temp = [];
    arr.forEach(item => {
      const index = _args.findIndex(x => x === _);
      if (index !== -1) {
        _args.splice(index, 1, item);
      } else {
        temp.push(item);
      }
    });
    _args.push(...temp);
    if (!checkLenth(_args, length)) {
      return curry.call(this, fn, _args);
    } else {
      return fn.apply(this, _args);
    }
  };
};

const addCurry = curry(add);
addCurry(1, _, 3)(2);
```

## **数组扁平化**

```javascript
function flatten(arr) {
  var result = [];
  for (var i = 0, len = arr.length; i < len; i++) {
    if (Array.isArray(arr[i])) {
      result = result.concat(flatten(arr[i]));
    } else {
      result.push(arr[i]);
    }
  }
  return result;
}

const flatten2 = arr => {
  return [].concat(...arr.map(x => (Array.isArray(x) ? flatten(x) : x)));
};
```

## **Promise 实现**

```javascript
new Promise((resolve, reject) => {
  resolve('1');
});

class Promise {
  constructor(exec) {
    // 检验输入值
    if (typeof exec !== 'function') {
      throw new TypeError(`Promise resolve ${exec} is not a function`);
    }

    this.initValue();

    try {
      exec(this.resolve, this.reject);
    } catch (e) {
      this.reject(e);
    }
  }

  // 初始值
  state = Promise.PENDING; // 状态
  value = null; // 终值
  reason = null; // 拒绝原因
  onFulfilledCallbacks = []; // 成功回调缓存数组
  onRejectedCallbacks = []; // 失败回调缓存数组

  initValue() {
    this.state = Promise.PENDING;
    this.value = null;
    this.reason = null;
    this.onFulfilledCallbacks = [];
    this.onRejectedCallbacks = [];
  }

  // 处理成功操作后一系列操作(状态改变，成功回调执行)
  resolve = value => {
    if (this.state == Promise.PENDING) {
      this.state = Promise.FULFILLED;
      this.value = value;
      this.onFulfilledCallbacks.forEach(fn => fn(this.value));
    }
  };

  // 处理失败操作后一系列操作(状态改变，失败回调执行)
  reject = reason => {
    if (this.state == Promise.PENDING) {
      this.state = Promise.REJECTED;
      this.reason = reason;
      this.onRejectedCallbacks.forEach(fn => fn(this.reason));
    }
  };

  /**
   * 实现链式调用，且改变了后面的then的值，必须通过新的实例
   */
  then(onFulfilled, onRejected) {
    // 检验参数
    if (typeof onFulfilled !== 'function') {
      onFulfilled = value => value;
    }
    if (typeof onRejected !== 'function') {
      onRejected = reason => reason;
    }

    if (this.state === Promise.FULFILLED) {
      setTimeout(() => {
        try {
          const x = onFulfilled(this.value);
          this.resolve(x);
        } catch (e) {
          this.reject(e);
        }
      });
    }
    if (this.state === Promise.REJECTED) {
      setTimeout(() => {
        try {
          const x = onRejected(this.reason);
          this.resolve(x);
        } catch (e) {
          this.reject(e);
        }
      });
    }
    if (this.state === Promise.PENDING) {
      this.onFulfilledCallbacks.push(value => {
        setTimeout(() => {
          try {
            const x = onFulfilled(value);
            this.resolve(x);
          } catch (e) {
            this.reject(e);
          }
        });
      });
      this.onRejectedCallbacks.push(reason => {
        setTimeout(() => {
          try {
            const x = onRejected(reason);
            this.resolve(x);
          } catch (e) {
            this.reject(e);
          }
        });
      });
    }
  }
  all(promises) {
    const values = Array.apply(null, { length: promises.length }); // 用来保存所有成功的 value 的数组
    let resolveCount = 0;

    return new Promise((resolve, reject) => {
      promises.forEach((p, index) => {
        Promise.resolve(p).then(
          res => {
            resolveCount++;
            values[index] = res; // 避免不按顺序执行完成
            if (resolveCount === promises.length) {
              resolve(values);
            }
          },
          err => {
            reject(err);
          }
        );
      });
    });
  }
}

// 优化点： 魔法数字变成常量
Promise.PENDING = 'pending';
Promise.FULFILLED = 'fulfilled';
Promise.REJECTED = 'rejected';
```

## **继承**

```javascript
function Person(name) {
  this.name = name || 'Loki';
}

Person.prototype.getName = function() {
  console.log(`this guy name is ${this.name}`);
};

/**
 * 1.原型链继承
 * 无法类似父类直接声明传参
 */
function Man() {
  this.age = 12;
}
Man.prototype = new Person();
Man.prototype.constructor = Man;

let man = new Man(99);
man.getName(); // 'this guy name is Loki'

man.constructor; // 不同

man.age; // 12 & 12

/**
 * 2.经典继承
 * 无法使用父类的方法
 */
function Woman(name) {
  Person.call(this, name);
}

let woman = new Woman('Lily');
woman.name; // Lily
woman.getName(); // TypeError

/**
 * 3.组合继承
 * 混合两者，解决两者的缺点
 */
function Zombie(name) {
  Person.call(this, name);
}
Zombie.prototype = new Person();

let zombie = new Zombie('Babo');
zombie.name; // Babo
zombie.getName(); // 'this guy name is Babo'

/**
 * 4.原型式继承
 * 模拟Es5 Object.create 的实现
 * 缺点 与原型链一样，所有子类的公共属性引用父类的
 */
function createObj(o) {
  function F() {}
  F.prototype = o;
  return new F();
}

var person = {
  name: 'kevin',
  friends: ['daisy', 'kelly']
};

var person1 = createObj(person);
person1.name;

// /**
//  * 5.寄生式继承
//  * 复制出父类原型副本
//  * 重新赋值constructor
//  */
function createObj(o) {
  // 第四点模拟实现
  var clone = Object.create(o);
  clone.sayName = function() {
    console.log('hi');
  };
  return clone;
}

/**
 * 6.寄生组合式继承
 */
function Parent() {
  this.name = 'loki';
}
function Child() {
  Parent.call(this);
  this.age = 18;
}
function object(o) {
  function F() {}
  F.prototype = o;
  return new F();
}

function prototype(child, parent) {
  var prototype = object(parent.prototype);
  prototype.constructor = child;
  child.prototype = prototype;
}

prototype(Child, Parent);

let child = new Child();
```

<!-- 新文章 -->
<!-- ES6模板字符串 -->

```javascript
function getInfo(a, b, c) {
  console.log(a);
  console.log(b);
  console.log(c);
  return a + b + c;
}
const a = 'loki';
const b = 24;

getInfo`test ${a} is boy, ${b}`;
```
