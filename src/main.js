import Vue from 'vue';
import App from './App.vue';

// element init
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

// Vue3.0 新Api
import VueCompositionApi from '@vue/composition-api';

// Vue 路由
import router from './router';

// Vue 组件
import './components';
// Anime动画脚本
import VueAnime from 'vue-animejs';

Vue.config.productionTip = false;

Vue.use(ElementUI);
Vue.use(VueCompositionApi);
Vue.use(VueAnime);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
