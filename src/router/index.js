import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '*',
    component: () => import('@/views/index/index')
  },
  {
    path: '/shiny-glass_1',
    component: () => import('@/views/shinyGlass/Demo1')
  },
  {
    path: '/Txt1',
    component: () => import('@/views/text/Demo1')
  },
  {
    path: '/Txt2',
    component: () => import('@/views/text/Demo2')
  },
  {
    path: '/Bg1',
    component: () => import('@/views/background/Demo1')
  },
  {
    path: '/Bg3',
    component: () => import('@/views/background/Demo3')
  },
  {
    path: '/Bg4',
    component: () => import('@/views/background/Demo4')
  },
  {
    path: '/Bg5',
    component: () => import('@/views/background/Demo5')
  },
  {
    path: '/Bg6',
    component: () => import('@/views/background/Demo6')
  },
  {
    path: '/Bg7',
    component: () => import('@/views/background/Demo7')
  },
  {
    path: '/Bg8',
    component: () => import('@/views/background/Demo8')
  },
  {
    path: '/Bg9',
    component: () => import('@/views/background/Demo9')
  },
  // {
  //   path: '/Bg10',
  //   component: () => import('@/views/background/Demo10')
  // },
  {
    path: '/Bg11',
    component: () => import('@/views/background/Demo11')
  },
  {
    path: '/Bg12',
    component: () => import('@/views/background/Demo12')
  },
  {
    path: '/player',
    component: () => import('@/views/player/index')
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  },
  {
    path: '/401',
    component: () => import('@/views/error-page/401'),
    hidden: true
  }
];

const createRouter = () =>
  new Router({
    mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });

const router = createRouter();

export default router;
