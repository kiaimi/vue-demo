export class Snow {
  _canvas;
  _ctx;
  _points = [];
  _maxDist = 100;

  constructor() {}

  init() {
    this._canvas = document.getElementById('canvas');
    this._ctx = this._canvas.getContext('2d');
    setTimeout(() => {
      this.resizeCanvas();
      this.generatePoints(700);
      this.pointFun();
      this.draw(this._points[0]);

      this.update(this._points[0]);

      setInterval(() => {
        this.pointFun();
      }, 25);
    }, 0);
    // window.addEventListener('resize', this.resizeCanvas, false);
  }

  point() {
    let temp = {};
    temp.x =
      Math.random() * (this._canvas.width + this._maxDist) - this._maxDist / 2;
    temp.y =
      Math.random() * (this._canvas.height + this._maxDist) - this._maxDist / 2;
    temp.z = Math.random() * 0.5 + 0.5;
    temp.vx = (Math.random() * 0.2 - 0.5) * temp.z;
    temp.vy = (Math.random() * 1.5 + 1.5) * temp.z;
    temp.fill = `rgba(255,255,255,${0.5 * Math.random() + 0.5}`;
    temp.dia = (Math.random() * 2.5 + 1.5) * temp.z;
    this._points.push(temp);
  }

  generatePoints(amount = 10) {
    for (let i = 0; i < amount; i++) {
      this.point();
    }
  }

  draw(obj) {
    this._ctx.beginPath();
    this._ctx.strokeStyle = 'transparent';
    this._ctx.fillStyle = obj.fill;
    this._ctx.arc(obj.x, obj.y, obj.dia, 0, 2 * Math.PI);
    this._ctx.closePath();
    this._ctx.stroke();
    this._ctx.fill();
  }

  update(obj) {
    obj.x += obj.vx;
    obj.y += obj.vy;
    if (obj.x > this._canvas.width + this._maxDist / 2) {
      obj.x = -(this._maxDist / 2);
    } else if (obj.xpos < -(this._maxDist / 2)) {
      obj.x = this._canvas.width + this._maxDist / 2;
    }
    if (obj.y > this._canvas.height + this._maxDist / 2) {
      obj.y = -(this._maxDist / 2);
    } else if (obj.y < -(this._maxDist / 2)) {
      obj.y = this._canvas.height + this._maxDist / 2;
    }
  }

  pointFun() {
    this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
    for (let i = 0; i < this._points.length; i++) {
      this.draw(this._points[i]);
      this.update(this._points[i]);
    }
  }

  resizeCanvas() {
    this._canvas.width = window.innerWidth;
    this._canvas.height = window.innerHeight;
    this.pointFun();
  }
}
