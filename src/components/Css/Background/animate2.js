import anime from 'animejs';

export function translate(element) {
  anime({
    targets: element,
    translateX: () => {
      return anime.random(-1000, 1000);
    },
    translateY: () => {
      return anime.random(-500, 500);
    },
    scale: () => {
      return anime.random(1, 5);
    },
    rotate: 45,
    easing: 'easeInOutBack',
    duration: 4 * 1000,
    delay: anime.stagger(10),
    // 重复发牌
    // loop: true
    complete: () => translate(element)
  });
}
