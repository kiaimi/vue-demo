const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');
const smp = new SpeedMeasurePlugin({
  outputFormat: 'human'
});
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
  .BundleAnalyzerPlugin;
module.exports = {
  productionSourceMap: false,
  chainWebpack: config => {
    // config.optimization.splitChunks({
    //   chunks: 'all',
    //   minSize: 30000,
    //   maxAsyncRequests: 5,
    //   maxInitialRequests: 3,
    //   automaticNameDelimiter: '~',
    //   name: true,
    //   cacheGroups: {
    //     commons: {
    //       name: 'common',
    //       test: /[\\/]node_modules[\\/]/,
    //       chunks: 'initial',
    //       priority: 10,
    //       minChunks: 2
    //     },
    //     elementUi: {
    //       name: 'element-ui',
    //       test: /[\\/]node_modules[\\/](element-ui)[\\/]/,
    //       chunks: 'all',
    //       priority: 20
    //     }
    //   }
    // });
  },
  css: {
    loaderOptions: {
      // 向 CSS 相关的 loader 传递选项
      less: {
        lessOptions: {
          javascriptEnabled: true
        }
      }
    }
  },
  configureWebpack: smp.wrap({
    plugins: [new BundleAnalyzerPlugin()]
  })
};
